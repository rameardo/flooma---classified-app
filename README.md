## Flooma

Flooma is an application to create free ads for your products and sell them in your area.


#### Backend Featuers

1. Dashboard such as Statistics etc..
2. Issues Manager
3. Administrator Manager, Roles and permissions
4. Users Manager
5. Blog Manager (Categor also)
6. Products Manager (Categor also)
7. Offers & Messages
8. Pages such as Privacy and Policy etc...
9. Languages Settings
10. Comments & Rating System
11. Settings such as maintenance etc..
12. Operational System Status
13. Ad Spaces 
14. Email Settings
15. General Settings Such as cookies message and app name etc...
16. Payments Settings
17. Visual Settings
18. Prefernce Settings
19. Currency Settings
20. Social Login Settings
21. Api Settings