<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('lang/{lang}', 'LanguageController@index');
Route::middleware(['Lang', 'Visitors'])->group(function () { 



	Route::post('/join', 'AuthenticationController@register')->name('join');
	Auth::routes();	

	// Front end
	Route::middleware(['Maintenance'])->group(function () { 
	
		Route::get('/', function () {
		    return view('welcome');
		});
		

		Route::get('/home', 'HomeController@index')->name('home');

		

	});


	/**
	* Admin Routing
	*/
	Route::group(['prefix' => ADMIN_PREFIX(),'middleware' => 'roles', 'roles' => ['Admin', 'Editor']], function(){

		Route::get('/', 'Backend\Dashboard\DashboardController@index')->name('Admin');

		// Route::get('/users', 'Backend\Users\UsersController@index')->name('Admin.users');
		Route::resource('/users', 'Backend\Users\UsersController', ['as' => 'admin']);
		Route::post('/users/update/account_settings', 'Backend\Users\UsersController@account_settings')->name('settings.account_settings');
		Route::get('/users/activity/{id}', 'Backend\Users\UsersController@showActivity')->name('user.activity.backend');

		Route::resource('/admins', 'Backend\Administrators\AdministratorsController', ['as' => 'admin']);


		// Languages
		Route::get('/languages', 'Backend\Settings\Languages\LanguagesController@index')->name('admin.languages.index');
		Route::get('/languages/translations/{lang}', 'Backend\Settings\Languages\LanguagesController@show')->name('admin.languages.show');
		Route::get('/languages/new', 'Backend\Settings\Languages\LanguagesController@newLang')->name('admin.languages.newLang');
		Route::get('/languages/{lang}', 'Backend\Settings\Languages\LanguagesController@editLang')->name('admin.languages.editLang');
		
		Route::post('/languages/set-default-language', 'Backend\Settings\Languages\LanguagesController@setDefaultLanguage')->name('admin.languages.setDefaultLanguage');

		Route::post('/languages/add-new-lang', 'Backend\Settings\Languages\LanguagesController@addNewLang')->name('admin.languages.addNewLang');

		Route::post('/languages/delete/{lang}', 'Backend\Settings\Languages\LanguagesController@deleteLang')->name('admin.languages.deleteLang');


	});

});


