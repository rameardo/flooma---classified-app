<!DOCTYPE html>
<html class="loading">
    <head>

    	{{-- Start Meta Tags --}}
	    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
	    <meta name="description" content="Frest admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
	    <meta name="keywords" content="admin template, Frest admin template, dashboard template, flat admin template, responsive admin template, web app">
	    <meta name="author" content="Ramear Do">
	    {{-- End Meta Tags --}}

	    {{-- Start Page Title --}}
	    <title>
	    	Hello
	    </title>
	    {{-- End Page Title --}}

	    {{-- Start Favicon --}}
	    <link rel="apple-touch-icon" href="../../../app-assets/images/ico/apple-icon-120.html">
	    <link rel="shortcut icon" type="image/x-icon" href="https://www.pixinvent.com/demo/frest-clean-bootstrap-admin-dashboard-template/app-assets/images/ico/favicon.ico">
	    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,600%7CIBM+Plex+Sans:300,400,500,600,700" rel="stylesheet">
	    {{-- End Favicon --}}

	    <!-- BEGIN: Vendor CSS-->
	    <link rel="stylesheet" type="text/css" href="{{asset('static/backend/app-assets/vendors/css/vendors.min.css')}}">
	    <link rel="stylesheet" type="text/css" href="{{asset('static/backend/app-assets/vendors/css/boxicons/css/boxicons.min.css')}}">
	    @yield('vendor-css')
	    <!-- END: Vendor CSS-->

	    <!-- BEGIN: Theme CSS-->
	    <link rel="stylesheet" type="text/css" href="{{asset('static/backend/app-assets/css/bootstrap.min.css')}}">
	    <link rel="stylesheet" type="text/css" href="{{asset('static/backend/app-assets/css/bootstrap-extended.min.css')}}">
	    <link rel="stylesheet" type="text/css" href="{{asset('static/backend/app-assets/css/colors.min.css')}}">
	    <link rel="stylesheet" type="text/css" href="{{asset('static/backend/app-assets/css/components.min.css')}}">
	    <!-- END: Theme CSS-->

	    <!-- BEGIN: Page CSS-->
	    <link rel="stylesheet" type="text/css" href="{{asset('static/backend/app-assets/css/core/menu/menu-types/vertical-menu.min.css')}}">
	    @yield('pages-css')
	    <!-- END: Page CSS-->



	    <!-- BEGIN: Custom CSS-->
	    @if (current_lang('dirction') == 'rtl')
	    	<link rel="stylesheet" type="text/css" href="{{asset('static/backend/assets/css/style.rtl.css')}}">

	    @else
	    	<link rel="stylesheet" type="text/css" href="{{asset('static/backend/assets/css/style.css')}}">
	    	
	    @endif	    
	    <!-- END: Custom CSS-->


    </head>
    <body class="vertical-layout vertical-menu-modern boxicon-layout no-card-shadow 2-columns  navbar-sticky footer-static  " data-open="click" data-menu="vertical-menu-modern" data-col="2-columns">


    	{{-- Start Header --}}
    	@include('Backend._layouts.header')
    	{{-- End Header --}}


    	{{-- Start Sidebar --}}
    	@include('Backend._layouts.sidebar')
    	{{-- End Sidebar --}}


	    <!-- BEGIN: Content-->
	    <div class="app-content content">
	      <div class="content-overlay"></div>
	      <div class="content-wrapper">
	        <div class="content-header row">
	        </div>
	        <div class="content-body">

	        		@yield('content')

	        </div>
	      </div>
	    </div>
	    <!-- END: Content-->



    	{{-- Start Footer --}}
    	@include('Backend._layouts.footer')
    	{{-- End Footer --}}







    <!-- BEGIN: Vendor JS-->
    <script src="{{asset('static/backend/app-assets/vendors/js/vendors.min.js')}}"></script>
    @yield('vendor-js')
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    @yield('vendor-js')
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="{{asset('static/backend/app-assets/js/core/app-menu.min.js')}}"></script>
    <script src="{{asset('static/backend/app-assets/js/core/app.min.js')}}"></script>
    <script src="{{asset('static/backend/app-assets/js/scripts/components.min.js')}}"></script>
    <script src="{{asset('static/backend/app-assets/js/scripts/footer.min.js')}}"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    @yield('pages-js')
    <!-- END: Page JS-->

    @yield('scripts')

    </body>
</html>
