    <!-- BEGIN: Main Menu-->
    <div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true">
      <!-- Start Flooma Logo -->
      <div class="navbar-header">
        <ul class="nav navbar-nav flex-row">
          <li class="nav-item mr-auto">
            <a class="navbar-brand" href="{{REQUEST_URI()}}">
              <div class="brand-logo"></div>
              <h2 class="brand-text mb-0">Flooma</h2>
            </a>
            </li>
          <li class="nav-item nav-toggle"><a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse"><i class="bx bx-x d-block d-xl-none font-medium-4 primary toggle-icon"></i><i class="toggle-icon bx bx-disc font-medium-4 d-none d-xl-block collapse-toggle-icon primary" data-ticon="bx-disc"></i></a></li>
        </ul>
      </div>
      <!-- End Flooma Logo -->

      <div class="shadow-bottom"></div>

      <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation" data-icon-style="">

          @if (Auth::user()->hasRole('Admin'))
          {{-- Start Dashboard --}}
          <li class="nav-item {{ isMenuActive(ADMIN_PREFIX(), 'active') }}">
            <a href="{{route('Admin')}}">
              <i class='bx bx-layer' ></i>
              <span class="menu-title" data-i18n="Email">{{__lang('dashboard')}}</span>
            </a>
          </li>
          {{-- End Dashboard --}}
          @endif



          <li class=" navigation-header">
            <span>{{__lang('user_management')}}</span>
          </li>

          <!-- Start Users -->
          <li class=" nav-item {{ isMenuActive(REQUEST_URI('users'), 'active') }}">
            <a href="#">
              {{-- <i class='bx bx-user' ></i> --}}
              <i class='bx bx-user-pin' ></i>
              <span class="menu-title">{{__lang('users')}}</span>
            </a>
            <ul class="menu-content">

              <li class="{{ isMenuActive(REQUEST_URI('users'), 'active') }}">
                <a href="{{route('admin.users.index')}}">
                  <i class="bx bx-right-arrow-alt"></i>
                  <span class="menu-item">{{__lang('users')}}</span>
                </a>
              </li>

              <li>
                <a href="#">
                  <i class="bx bx-right-arrow-alt"></i>
                  <span class="menu-item">{{__lang('pending_users')}}</span>
                </a>
              </li>

              <li>
                <a href="#">
                  <i class="bx bx-right-arrow-alt"></i>
                  <span class="menu-item">{{__lang('deleted_users')}}</span>
                </a>
              </li>

            </ul>
          </li>
          <!-- End Users -->

          <!-- Start Products -->
          <li class=" nav-item">
            <a href="#">
              <i class='bx bx-shopping-bag' ></i>
              <span class="menu-title">{{__lang('products')}}</span>
            </a>
            <ul class="menu-content">

              <li>
                <a href="/blog/posts">
                  <i class="bx bx-right-arrow-alt"></i>
                  <span class="menu-item">{{__lang('products')}}</span>
                </a>
              </li>

              <li>
                <a href="#">
                  <i class="bx bx-right-arrow-alt"></i>
                  <span class="menu-item">{{__lang('categories')}}</span>
                </a>
              </li>

              <li>
                <a href="#">
                  <i class="bx bx-right-arrow-alt"></i>
                  <span class="menu-item">{{__lang('sold_products')}}</span>
                </a>
              </li>

              <li>
                <a href="#">
                  <i class="bx bx-right-arrow-alt"></i>
                  <span class="menu-item">{{__lang('pending_products')}}</span>
                </a>
              </li>

              <li>
                <a href="#">
                  <i class="bx bx-right-arrow-alt"></i>
                  <span class="menu-item">{{__lang('hidden_products')}}</span>
                </a>
              </li>

              <li>
                <a href="#">
                  <i class="bx bx-right-arrow-alt"></i>
                  <span class="menu-item">{{__lang('deleted_products')}}</span>
                </a>
              </li>
            </ul>
          </li>
          <!-- End Products -->

          <!-- Start Shops -->
          <li class=" nav-item">
            <a href="#">
              <i class='bx bx-strikethrough' ></i>
              <span class="menu-title">{{__lang('shops')}}</span>
            </a>
            <ul class="menu-content">

              <li>
                <a href="/blog/posts">
                  <i class="bx bx-right-arrow-alt"></i>
                  <span class="menu-item">{{__lang('shops')}}</span>
                </a>
              </li>

              <li>
                <a href="#">
                  <i class="bx bx-right-arrow-alt"></i>
                  <span class="menu-item">{{__lang('pending_shops')}}</span>
                </a>
              </li>

              <li>
                <a href="#">
                  <i class="bx bx-right-arrow-alt"></i>
                  <span class="menu-item">{{__lang('deleted_shops')}}</span>
                </a>
              </li>

            </ul>
          </li>
          <!-- End Shops -->

          <!-- Start Reviews -->
          <li class=" nav-item">
            <a href="#">
              <i class='bx bx-star' ></i>
              <span class="menu-title">{{__lang('reviews')}}</span>
            </a>
            <ul class="menu-content">

              <li>
                <a href="/blog/posts">
                  <i class="bx bx-right-arrow-alt"></i>
                  <span class="menu-item">{{__lang('reviews')}}</span>
                </a>
              </li>

              <li>
                <a href="#">
                  <i class="bx bx-right-arrow-alt"></i>
                  <span class="menu-item">{{__lang('deleted_reviews')}}</span>
                </a>
              </li>

            </ul>
          </li>
          <!-- End Reviews -->

          <!-- Start Comments -->
          <li class=" nav-item">
            <a href="#">
              <i class='bx bx-comment' ></i>
              <span class="menu-title">{{__lang('comments')}}</span>
            </a>
            <ul class="menu-content">

              <li>
                <a href="/blog/posts">
                  <i class="bx bx-right-arrow-alt"></i>
                  <span class="menu-item">{{__lang('comments')}}</span>
                </a>
              </li>

              <li>
                <a href="#">
                  <i class="bx bx-right-arrow-alt"></i>
                  <span class="menu-item">{{__lang('deleted_comments')}}</span>
                </a>
              </li>

            </ul>
          </li>
          <!-- End Comments -->

          <!-- Start Messages -->
          <li class=" nav-item">
            <a href="#">
              {{-- <i class='bx bx-chat'></i> --}}
              <i class='bx bx-message-alt-dots' ></i>
              <span class="menu-title">{{__lang('messages')}}</span>
            </a>
            <ul class="menu-content">

              <li>
                <a href="">
                  <i class="bx bx-right-arrow-alt"></i>
                  <span class="menu-item">{{__lang('messages')}}</span>
                </a>
              </li>

              <li>
                <a href="#">
                  <i class="bx bx-right-arrow-alt"></i>
                  <span class="menu-item">{{__lang('deleted_messages')}}</span>
                </a>
              </li>
            </ul>
          </li>
          <!-- End Messages -->

          <!-- Start Offers -->
          <li class="nav-item">
            <a href="">
              <i class='bx bx-euro' ></i>
              <span class="menu-title" data-i18n="Email">{{__lang('offers')}}</span>
            </a>
          </li>
          <!-- End Offers -->


          <li class=" navigation-header">
            <span>{{__lang('site_management')}}</span>
          </li>


          <!-- Start Administrators -->
          <li class="nav-item {{ isMenuActive(REQUEST_URI('admins'), 'active') }}">
            <a href="#">
              <i class='bx bx-user' ></i>
              <span class="menu-title">{{__lang('administrators')}}</span>
            </a>
            <ul class="menu-content">

              <li class="{{ isMenuActive(REQUEST_URI('admins'), 'active') }}">
                <a href="{{route('admin.admins.index')}}">
                  <i class="bx bx-right-arrow-alt"></i>
                  <span class="menu-item">{{__lang('administrators')}}</span>
                </a>
              </li>

              <li>
                <a href="#">
                  <i class="bx bx-right-arrow-alt"></i>
                  <span class="menu-item">{{__lang('pending_admins')}}</span>
                </a>
              </li>

              <li>
                <a href="#">
                  <i class="bx bx-right-arrow-alt"></i>
                  <span class="menu-item">{{__lang('deleted_admins')}}</span>
                </a>
              </li>

            </ul>
          </li>
          <!-- End Administrators -->

          <!-- Start Blog -->
          <li class=" nav-item">
            <a href="#">
              <i class='bx bx-book-open' ></i>
              <span class="menu-title">{{__lang('blog')}}</span>
            </a>
            <ul class="menu-content">

              <li>
                <a href="/blog/posts">
                  <i class="bx bx-right-arrow-alt"></i>
                  <span class="menu-item">{{__lang('articles')}}</span>
                </a>
              </li>

              <li>
                <a href="#">
                  <i class="bx bx-right-arrow-alt"></i>
                  <span class="menu-item">{{__lang('categories')}}</span>
                </a>
              </li>

              <li>
                <a href="#">
                  <i class="bx bx-right-arrow-alt"></i>
                  <span class="menu-item">{{__lang('tags')}}</span>
                </a>
              </li>

            </ul>
          </li>
          <!-- End Blog -->

          <!-- Start Pages -->
          <li class="nav-item">
            <a href="">
              <i class='bx bx-credit-card-alt' ></i>
              <span class="menu-title" data-i18n="Email">{{__lang('pages')}}</span>
            </a>
          </li>
          <!-- End Pages -->


          <!-- Start Newslatter -->
          <li class="nav-item">
            <a href="">
              <i class='bx bx-envelope' ></i>
              <span class="menu-title" data-i18n="Email">{{__lang('newslatter')}}</span>
            </a>
          </li>
          <!-- End Newslatter -->


          <!-- Start Contact Messages -->
          <li class="nav-item">
            <a href="">
              <i class='bx bx-message-alt' ></i>
              <span class="menu-title" data-i18n="Email">{{__lang('contact_messages')}}</span>
            </a>
          </li>
          <!-- End Contact Messages -->


          {{-- Start Technical --}}
          <li class=" navigation-header">
            <span>{{__lang('technical')}}</span>
          </li>

          <li class="nav-item">
            <a href="">
              {{-- <i class='bx bx-book-open' ></i> --}}
              <i class='bx bx-git-branch' ></i>
              <span class="menu-title" data-i18n="Email">{{__lang('project_issues')}}</span>
            </a>
          </li>

          <li class="nav-item">
            <a href="">
              <i class='bx bx-task' ></i>
              <span class="menu-title" data-i18n="Email">{{__lang('administrators_todo')}}</span>
            </a>
          </li>

          <li class="nav-item">
            <a href="">
              {{-- <i class='bx bx-recycle' ></i> --}}
              <i class='bx bx-code-curly' ></i>
              <span class="menu-title" data-i18n="Email">{{__lang('system_operational')}}</span>
            </a>
          </li>

          {{-- End Technical --}}


   
          {{-- Start Settings --}}
          <li class=" navigation-header">
            <span>{{__lang('settings')}}</span>
          </li>


          <li class=" nav-item {{ isMenuActive(REQUEST_URI('languages'), 'open') }}">
            <a href="#Flooma->Settings">
              <i class='bx bx-cog'></i>
              <span class="menu-title">{{__lang('settings')}}</span>
            </a>
            <ul class="menu-content">

              <li>
                <a href="/blog/posts">
                  <i class="bx bx-right-arrow-alt"></i>
                  <span class="menu-item">{{__lang('settings')}}</span>
                </a>
              </li>

              <li>
                <a href="#">
                  <i class="bx bx-right-arrow-alt"></i>
                  <span class="menu-item">{{__lang('general_settings')}}</span>
                </a>
              </li>

              <li class="{{ isMenuActive(REQUEST_URI('languages'), 'active') }}">
                <a href="{{route('admin.languages.index')}}">
                  <i class="bx bx-right-arrow-alt"></i>
                  <span class="menu-item">{{__lang('languages_settings')}}</span>
                </a>
              </li>

              <li>
                <a href="#">
                  <i class="bx bx-right-arrow-alt"></i>
                  <span class="menu-item">{{__lang('email_settings')}}</span>
                </a>
              </li>

              <li>
                <a href="#">
                  <i class="bx bx-right-arrow-alt"></i>
                  <span class="menu-item">{{__lang('payments_settings')}}</span>
                </a>
              </li>

              <li>
                <a href="#">
                  <i class="bx bx-right-arrow-alt"></i>
                  <span class="menu-item">{{__lang('visual_settings')}}</span>
                </a>
              </li>

              <li>
                <a href="#">
                  <i class="bx bx-right-arrow-alt"></i>
                  <span class="menu-item">{{__lang('prefernce_settings')}}</span>
                </a>
              </li>

              <li>
                <a href="#">
                  <i class="bx bx-right-arrow-alt"></i>
                  <span class="menu-item">{{__lang('currency_settings')}}</span>
                </a>
              </li>

              <li>
                <a href="#">
                  <i class="bx bx-right-arrow-alt"></i>
                  <span class="menu-item">{{__lang('social_login_settings')}}</span>
                </a>
              </li>

              <li>
                <a href="#">
                  <i class="bx bx-right-arrow-alt"></i>
                  <span class="menu-item">{{__lang('api_settings')}}</span>
                </a>
              </li>

            </ul>
          </li>


          {{-- End Settings --}}



        </ul>
      </div>
    </div>
    <!-- END: Main Menu-->