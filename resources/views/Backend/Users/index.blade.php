{{-- Extends The Root --}}
@extends('backend.root')

{{-- Start Css Vendors --}}
@section('vendor-css')
<link rel="stylesheet" type="text/css" href="{{asset('static/backend/app-assets/vendors/css/tables/datatable/datatables.min.css')}}">
@stop {{-- End Css Vendors --}}


{{-- Start Pages Ttitle --}}

@section('pages-title')
Dashboard
@stop

{{-- End Pages Ttitle --}}





{{-- Start Page Content --}}
@section('content')

@if ($users_counter == 0)


 <section class="empty-records">
     <img class="no-data-image" src="{{asset('static/backend/assets/images/errors/no_data.svg')}}">
     <div class="no-data-msg">There are no users yet!</div>
 </section>


@else

<!-- Zero configuration table -->
<section id="column-selectors">
    <div class="row">
        <div class="col-12">
            <div class="card flooma-card-table">
                <div class="card-header d-flex justify-content-between ">
                    <h4 class="font-large-1">Users</h4>
										<a href="#" class="btn btn-light-secondary glow mr-1 mb-1">
											<i class='bx bx-plus'></i>
											New user
                		</a>
                </div>
                <div class="card-content">
                    <div class="card-body card-dashboard">

                        <div class="table-responsive flooma-table">
                            <table class="table table-striped dataex-html5-selectors">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Username</th>
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>E-Mail Address</th>
                                        <th>Status</th>
                                        <th>Register date</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                	@foreach($users as $user)
                                    <tr>
                                        @if ($user->isOnline())
                                            Yes
                                        @else
                                            No                                        
                                        @endif
                                        
                                    	<td>{{$user->id}}</td>
 
                                        <td class="py-1 line-ellipsis">
                                            <span class="avatar m-0 custom-online-avatar">


                                            @if ($user->profile_image === null)

                                                <img class="rounded-circle mr-1" src="{{asset('static/public/user/default.png')}}" alt="{{$user->first_name}} {{$user->last_name}} image" height="24" width="24">

                                            @else

                                            <img class="rounded-circle mr-1" src="{{asset('static/public/user/')}} {{$user->profile_image}}" alt="{{$user->first_name}} {{$user->last_name}} image" height="24" width="24">

                                            @endif

                                            @if ($user->isOnline())
                                                <span class="avatar-status-online table-online-status tooltip-light"
                                                data-toggle="tooltip" 
                                                data-placement="top"
                                                data-original-title="Online"></span>
                                            @else
                                                <span class="avatar-status-offline table-online-status tooltip-light"
                                                data-toggle="tooltip" 
                                                data-placement="top"
                                                data-original-title="Offline"></span>
                                            @endif

                                                
                                            </span>                                          
                                          {{$user->name}}
                                        </td>     

                                        <td>
                                            @if ($user->first_name === null)
                                            <span style="cursor: pointer;" 
                                            class="tooltip-light" 
                                            data-toggle="tooltip" 
                                            data-placement="top"
                                            data-original-title="Not available">
                                                N/A
                                            </span>                                            
                                                
                                            @else
                                               {{$user->first_name}}
                                            @endif
                                        </td>
 

                                        <td>
                                            @if ($user->last_name === null)
                                            <span style="cursor: pointer;" 
                                            data-toggle="tooltip" 
                                            data-placement="top" title="" 
                                            data-original-title="Not available">
                                                N/A
                                            </span>                                            
                                                
                                            @else
                                               {{$user->last_name}}
                                            @endif
                                        </td>
 

                                        <td>
                                            {{$user->email}}
                                            @if ($user->email_verified_at === null)
                                                <i class='bx bx-x-circle' 
                                                style="cursor: pointer;position: absolute;margin-left: 5px;margin-top: 1px;" 
                                                data-trigger="hover"  
                                                data-toggle="popover" 
                                                data-placement="top" 
                                                data-container="body" 
                                                data-original-title="{{$user->email}}" 
                                                data-content="This E-Mail Address is Not Verified"></i>

     
                                            @else
                                                <i class='bx bx-check-circle' 
                                                style="cursor: pointer;position: absolute;margin-left: 5px;margin-top: 1px;" 
                                                data-trigger="hover"  
                                                data-toggle="popover" 
                                                data-placement="top" 
                                                data-container="body" 
                                                data-original-title="{{$user->email}}" 
                                                data-content="This E-Mail Address is Verified"></i>                                            
                                            @endif
                                            
                                        </td>
 {{--                                        <td>Edinburgh</td>
                                        <td class="text-success py-1">Active</td> --}}

                                        <td>

                                          @if ($user->is_active == 0)
                                            <span class="badge badge-light-danger badge-pill">Inactive</span>
                                          @elseif($user->is_active == 1)
                                            <span class="badge badge-light-secondary badge-pill">Active</span>
                                          @elseif($user->is_active == 2)
                                            <span class="badge badge-light-warning badge-pill">Pending</span>
                                          @else
                                            <span class="badge badge-light-secondary badge-pill">N/A</span>
                                          @endif

                                            
                                            
                                        </td>

                                        <td>
                                            <span style="cursor: pointer;" 
                                            data-toggle="tooltip" 
                                            data-placement="top" title="" 
                                            data-original-title="{{$user->created_at->format('d.m.Y')}}">
                                                {{$user->created_at->diffForHumans()}}
                                            </span>                                             
                                        </td>

                                        <td>


                                            <a href="{{route('admin.users.index')}}/{{$user->id}}" 
                                            class="btn flooma-btn btn-icon btn-light-secondary"
                                            data-toggle="tooltip" 
                                            data-placement="top" title="" 
                                            data-original-title="View">
                                                <i class='bx bx-show-alt'></i>
                                            </a>

                                            <a href="{{route('admin.users.index')}}/{{$user->id}}/edit" 
                                            class="btn flooma-btn btn-icon btn-light-secondary"
                                            data-toggle="tooltip" 
                                            data-placement="top" title="" 
                                            data-original-title="Edit">
                                                <i class='bx bx-edit-alt'></i>
                                            </a>

                       
                      
                                        	<a href="{{$user->id}}" class="btn flooma-btn btn-icon btn-light-secondary" data-toggle="modal" data-target="#confirmationDelete">
                                        		<i class='bx bx-trash' ></i>
                                        	</a>
                                        	
                                        </td>
                                    </tr>

    					

                                   @endforeach
                                </tbody>

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--/ Zero configuration table -->

@endif {{-- End if of Counter --}}


@stop





{{-- Start Js Vendors --}}
@section('vendor-js')
<script src="{{asset('static/backend/app-assets/vendors/js/tables/datatable/datatables.min.js')}}"></script>
<script src="{{asset('static/backend/app-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('static/backend/app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('static/backend/app-assets/vendors/js/tables/datatable/buttons.html5.min.js')}}"></script>
<script src="{{asset('static/backend/app-assets/vendors/js/tables/datatable/buttons.print.min.js')}}"></script>
<script src="{{asset('static/backend/app-assets/vendors/js/tables/datatable/pdfmake.min.js')}}"></script>
<script src="{{asset('static/backend/app-assets/vendors/js/tables/datatable/vfs_fonts.js')}}"></script>
@stop{{-- End Js Vendors --}}

{{-- Start Js pages --}}
@section('pages-js')
<script src="{{asset('static/backend/app-assets/js/scripts/datatables/datatable.min.js')}}"></script>
<script src="{{asset('static/backend/app-assets/js/scripts/modal/components-modal.min.js')}}"></script>
<script src="{{asset('static/backend/app-assets/js/scripts/popover/popover.min.js')}}"></script>
@stop{{-- End Js pages --}}
