{{-- Extends The Root --}}
@extends('backend.root')

{{-- Start Css Vendors --}}
@section('vendor-css')

@stop {{-- End Css Vendors --}}


{{-- Start Pages Ttitle --}}

@section('pages-title')
Dashboard
@stop

{{-- End Pages Ttitle --}}





{{-- Start Page Content --}}
@section('content')




<section class="page-user-profile">
  <div class="row">
    <div class="col-12">


      <!-- user profile content section start -->
      <div class="row">
        <!-- user profile nav tabs content start -->
        <div class="col-lg-9">

          <div class="s_">

            <div class="tab-pane" id="profile" aria-labelledby="profile-tab" role="tabpanel">
              <!-- user profile nav tabs profile start -->

              <div class="card">
                <div class="card-content">
                  <div class="card-body">
                    <h5 class="card-title">
                        {{$user->first_name}} {{$user->last_name}} activity


                    
                      <a href="{{route('admin.users.show', $user->id)}}" class="btn btn-sm d-none d-sm-block float-right btn-light-secondary mb-2">
                        <i class="cursor-pointer bx bx-chevron-left font-small-3 mr-50"></i><span>Back</span> 
                      </a>
                      <a href="{{route('admin.users.show', $user->id)}}" class="btn btn-sm d-block d-sm-none btn-block text-center btn-light-secondary">
                        <i class="cursor-pointer bx bx-chevron-left font-small-3 mr-25"></i><span>Back</span>
                      </a>      
                    
  

                    </h5>

                    {{-- Handle the Activity table --}}
                    @if ($activity_count == 0 || $activity_count < 1)
                      <div class="no-record-sub">
                        <img src="{{asset('static/backend/assets/images/no/no-records.png')}}">
                        <h4>{{$user->first_name}} {{$user->last_name}} has no activities yet!</h4>
                      </div>
                    @endif

                    <ul class="widget-timeline mb-0">
                      @foreach ($user->activities as $activity)
                
                                              
                        <li class="timeline-items timeline-icon-secondary active">
                          @if ($activity->created_at !== null)
                            <div class="timeline-time">{{$activity->created_at->format('F, j ')}}</div>
                          @endif
                          
                          <h6 class="timeline-title">
                            
                            @if ($activity->signal == 'comment')
                              <a href="{{$activity->first_value_link}}">{{$activity->first_value}}</a>
                               reacted to
                              <a href="{{$activity->second_value_link}}">{{$activity->second_value}}</a>s <a href="{{$activity->activity_url}}">Post</a>
                            @endif

                            @if ($activity->signal == 'join')
                              <a href="#" target="_blank">{{$activity->first_value}}</a> joined Flooma
                            @endif

                          </h6>

                          @if ($activity->created_at !== null)
                            <p class="timeline-text">{{$activity->created_at->diffForHumans()}}</p>
                          @endif      

                          @if ($activity->signal !== null)
                            @if ($activity->signal == 'comment')
                              <div class="timeline-content">
                                <img src="{{asset('static/backend/app-assets/images/icon/pdf.png')}}" alt="document" height="23" width="19" class="mr-50">
                                  New Order.pdf
                              </div>
                            @endif

                            
                          @endif
                        </li>

                      @endforeach
      
                    </ul>  

                  </div>
                </div>
              </div>
              <!-- user profile nav tabs profile ends -->
            </div>
          </div>



        </div>
        <!-- user profile nav tabs content ends -->
        <!-- user profile right side content start -->
        <div class="col-lg-3">

          <div class="simple-statistic-backend card">
            <div class="d-flex">

              <div class="col-md-6 ssb-section">
                <p>Total Sales</p>
                <h3>51</h3>
              </div>
              
              <div class="col-md-6 ssb-section">
                <p>Total Earnings</p>
                <h3>$4.7K</h3>
              </div>

            </div>
          </div>

          <!-- user profile right side content related groups start -->
          <div class="card">
            <div class="card-content">
              <div class="card-body">
                <h6 class="card-title mb-1">
                    <small>
                        Last 3 Products from {{$user->name}}
                    </small>              
                </h6>

                <div class="media d-flex align-items-center mb-1">
                  <a href="JavaScript:void(0);">
                    <img src="{{asset('static/backend/app-assets/images/banner/banner-30.jpg')}}" class="rounded" alt="group image"
                      height="64" width="64" />
                  </a>
                  <div class="media-body ml-1">
                    <h6 class="media-heading mb-0"><small>Play Guitar</small></h6><small class="text-muted">2.8k
                      members (7 joined)</small>
                  </div>
                  <i class="cursor-pointer bx bx-show-alt text-primary d-flex align-items-center"></i>
                </div>

                <div class="media d-flex align-items-center mb-1">
                  <a href="JavaScript:void(0);">
                    <img src="{{asset('static/backend/app-assets/images/banner/banner-30.jpg')}}" class="rounded" alt="group image"
                      height="64" width="64" />
                  </a>
                  <div class="media-body ml-1">
                    <h6 class="media-heading mb-0"><small>Play Guitar</small></h6><small class="text-muted">2.8k
                      members (7 joined)</small>
                  </div>
                  <i class="cursor-pointer bx bx-show-alt text-primary d-flex align-items-center"></i>
                </div>
                
                <div class="media d-flex align-items-center mb-1">
                  <a href="JavaScript:void(0);">
                    <img src="{{asset('static/backend/app-assets/images/banner/banner-30.jpg')}}" class="rounded" alt="group image"
                      height="64" width="64" />
                  </a>
                  <div class="media-body ml-1">
                    <h6 class="media-heading mb-0"><small>Play Guitar</small></h6><small class="text-muted">2.8k
                      members (7 joined)</small>
                  </div>
                  <i class="cursor-pointer bx bx-show-alt text-primary d-flex align-items-center"></i>
                </div>

              </div>
            </div>
          </div>
          <!-- user profile right side content related groups ends -->


        </div>
        <!-- user profile right side content ends -->
      </div>
      <!-- user profile content section start -->
    </div>
  </div>
</section>

@stop





{{-- Start Js Vendors --}}
@section('vendor-js')

@stop{{-- End Js Vendors --}}

{{-- Start Js pages --}}
@section('pages-js')

<script src="{{asset('static/backend/app-assets/js/scripts/modal/components-modal.min.js')}}"></script>
<script src="{{asset('static/backend/app-assets/js/scripts/popover/popover.min.js')}}"></script>
@stop{{-- End Js pages --}}
