{{-- Extends The Root --}}
@extends('backend.root')

{{-- Start Css Vendors --}}
@section('vendor-css')

<style>
  .switch-icon-right{right: 0px !important;}
</style>
@stop {{-- End Css Vendors --}}


{{-- Start Pages Ttitle --}}

@section('pages-title')
Dashboard
@stop

{{-- End Pages Ttitle --}}





{{-- Start Page Content --}}
@section('content')
<div class="row pills-stacked">
  <div class="col-md-3 col-sm-12">
    <ul class="nav nav-pills flex-column text-center text-md-left">
      <li class="nav-item">
        <a class="nav-link d-flex align-items-center active" id="stacked-pill-1" data-toggle="pill" href="#basic_information" aria-expanded="true">
          {{-- <i class="bx bx-link mr-25"></i> --}}
          <i class='bx bx-user-pin mr-50'></i>
          Basic Informations
        </a>
      </li>

       <li class="nav-item">
        <a class="nav-link d-flex align-items-center" id="stacked-pill-5" data-toggle="pill" href="#shop_informations" aria-expanded="true">
          {{-- <i class="bx bx-link mr-25"></i> --}}
          <i class='bx bx-strikethrough mr-50'></i>
          Shop Informations
        </a>
      </li>
      
      <li class="nav-item">
        <a class="nav-link d-flex align-items-center" id="stacked-pill-2" data-toggle="pill" href="#account_settings" aria-expanded="false">
          <i class='bx bx-cog mr-50' ></i>
          Account Settings
        </a>
      </li>

      <li class="nav-item">
        <a class="nav-link d-flex align-items-center" id="stacked-pill-3" data-toggle="pill" href="#preferences" aria-expanded="false">
          <i class='bx bx-ball mr-50' ></i>
          Preferences
        </a>
      </li>
      
      <li class="nav-item">
        <a class="nav-link d-flex align-items-center" id="stacked-pill-3" data-toggle="pill" href="#notifications" aria-expanded="false">
          <i class='bx bx-bell mr-50'></i>
          Notifications
        </a>
      </li>

    </ul>
  </div>
  <div class="col-md-9 col-sm-12">
    <div class="card">
      <div class="tab-content">
      
        {{-- Start Account Information --}}
        <div role="tabpanel" class="tab-pane active" id="basic_information" aria-labelledby="stacked-pill-1" aria-expanded="true">


          <div class="card-body">

            <h5 class="mb-1">
              <i class="bx bx-user-pin mr-25"></i>
              Basic Informations
            </h5>

            <div class="media">
                <a href="javascript: void(0);">

                  @if ($user->profile_image === null)

                      <img style="border-radius: 4px;" src="{{asset('static/public/user/default.png')}}" alt="{{$user->first_name}} {{$user->last_name}} image" height="64" width="64">

                  @else

                  <img style="border-radius: 4px;" src="{{asset('static/public/user/')}} {{$user->profile_image}}" alt="{{$user->first_name}} {{$user->last_name}} image" height="64" width="64">

                  @endif
                  

                </a>
                <div class="media-body mt-25">
                    <div class="col-12 px-0 d-flex flex-sm-row flex-column justify-content-start">
                        <button id="select-files" class="btn btn-sm btn-light-primary ml-50 mb-50 mb-sm-0 dz-clickable">
                          Change
                        </button>
                        <form hidden="" action="#" class="dropzone dropzone-area dz-clickable" id="dpz-btn-select-files">
                            <!-- <div class="dz-message">Drop Files Here To Upload</div> -->
                          <div class="dz-default dz-message">
                            <span>Drop files here to upload</span>
                          </div>
                        </form>

                        <button class="btn btn-sm btn-light-secondary ml-50">
                          Set to default {{config('app.name')}} image
                        </button>
                    </div>
                    <p class="text-muted ml-1 mt-50">
                      <small>Allowed JPG, JPEG or PNG. Max size of 2MB</small>
                    </p>
                </div>
                <a href="#" class="btn btn-outline-danger">Delete this account</a>
            </div>

            <hr>
            
            <form novalidate="">
                <div class="row">

                    <div class="col-md-6 col-12">
                        <div class="form-group">
                            <div class="controls">
                                <label>First Name</label>
                                <div class="position-relative has-icon-left">
                                  <input type="text" class="form-control" placeholder="Username" value="{{$user->first_name}}" required="" data-validation-required-message="This username field is required">
                                  <div class="form-control-position">
                                    <i class="bx bx-user-pin"></i>
                                  </div>
                                </div>                                
                            <div class="help-block"></div></div>
                        </div>
                    </div>

                  
                    <div class="col-md-6 col-12">
                        <div class="form-group">
                            <div class="controls">
                                <label>Last Name</label>
                                <div class="position-relative has-icon-left">
                                  <input type="text" class="form-control" placeholder="Username" value="{{$user->last_name}}" required="" data-validation-required-message="This username field is required">
                                  <div class="form-control-position">
                                    <i class="bx bx-user-pin"></i>
                                  </div>
                                </div>                                
                            <div class="help-block"></div></div>
                        </div>
                    </div>

                    <div class="col-md-6 col-12">
                        <div class="form-group">
                            <div class="controls">
                                <label>Username</label>
                                <div class="position-relative has-icon-left">
                                  <input type="text" class="form-control" placeholder="Username" value="{{$user->name}}" required="" data-validation-required-message="This username field is required">
                                  <div class="form-control-position">
                                    <i class="bx bx-user"></i>
                                  </div>
                                </div>                                
                            <div class="help-block"></div></div>
                        </div>
                    </div>

                  
                    <div class="col-md-6 col-12">
                        <div class="form-group">
                            <div class="controls">
                                <label>Phone Number</label>
                                <div class="position-relative has-icon-left">
                                  <input type="text" class="form-control" placeholder="Phone Number" value="{{$user->phone_number}}" required="" data-validation-required-message="This username field is required">
                                  <div class="form-control-position">
                                    <i class="bx bx-phone"></i>
                                  </div>
                                </div>                                
                            <div class="help-block"></div></div>
                        </div>
                    </div>


                    <div class="col-12">
                        <div class="form-group">
                            <div class="controls">
                                <label>E-Mail Address</label>
                                <div class="position-relative has-icon-left">
                                  <input type="text" class="form-control" placeholder="Username" value="{{$user->email}}" required="" data-validation-required-message="This username field is required">
                                  <div class="form-control-position">
                                    <i class="bx bx-envelope "></i>
                                  </div>
                                </div>                                
                            <div class="help-block"></div></div>
                        </div>
                    </div>

                    @if ($user->email_verified_at === null)
                      <div class="col-12">
                        <div class="alert bg-rgba-secondary alert-dismissible mb-2" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                          </button>
                          <div class="d-flex align-items-center">
                            <i class="bx bx-star"></i>
                            <span>
                              This email is not confirmed, <a href="javascript: void(0);">Resend confirmation</a>
                            </span>
                          </div>
                        </div>
                      </div>
                    @endif

                    <div class="col-12">
                        <div class="form-group">
                            <div class="controls">
                                <label>Administrators Notize</label>
                                <div class="position-relative has-icon-left">
                                  <textarea class="form-control" id="basicTextarea" rows="3" placeholder="The modification will be added by {{auth()->user()->name}}"></textarea>
                                  <div class="form-control-position">
                                    <i class="bx bx-info-circle"></i>
                                  </div>
                                </div>                                
                            <div class="help-block"></div></div>
                        </div>
                    </div>                    

                    <div class="col-12 d-flex flex-sm-row flex-column justify-content-end">

                        <button type="button" class="btn btn-light-secondary">Save</button>
                    </div>
                </div>
            </form>


          </div>
          {{-- End Account Information --}}


        </div> {{-- End Account Information --}}
     
        {{-- Start Shop Information --}}
        <div role="tabpanel" class="tab-pane" id="shop_informations" aria-labelledby="stacked-pill-5" aria-expanded="true">


          <div class="card-body">

            <h5 class="mb-1">
              <i class="bx bx-strikethrough mr-25"></i>
              Shops Informations
            </h5>

            <div class="media">
                <a href="javascript: void(0);">

                  @if ($user->profile_image === null)

                      <img style="border-radius: 4px;" src="{{asset('static/public/user/default.png')}}" alt="{{$user->first_name}} {{$user->last_name}} image" height="64" width="64">

                  @else

                  <img style="border-radius: 4px;" src="{{asset('static/public/user/')}} {{$user->profile_image}}" alt="{{$user->first_name}} {{$user->last_name}} image" height="64" width="64">

                  @endif
                  

                </a>
                <div class="media-body mt-25">
                    <div class="col-12 px-0 d-flex flex-sm-row flex-column justify-content-start">
                        <button id="select-files" class="btn btn-sm btn-light-primary ml-50 mb-50 mb-sm-0 dz-clickable">
                          Change
                        </button>
                        <form hidden="" action="#" class="dropzone dropzone-area dz-clickable" id="dpz-btn-select-files">
                            <!-- <div class="dz-message">Drop Files Here To Upload</div> -->
                          <div class="dz-default dz-message">
                            <span>Drop files here to upload</span>
                          </div>
                        </form>

                        <button class="btn btn-sm btn-light-secondary ml-50">
                          Set to default {{config('app.name')}} image
                        </button>
                    </div>
                    <p class="text-muted ml-1 mt-50">
                      <small>Allowed JPG, JPEG or PNG. Max size of 2MB</small>
                    </p>
                </div>

                <a href="#" class="btn btn-outline-danger">Delete this shop</a>

            </div>

            <hr>
            
            <form novalidate="">
                <div class="row">

                    <div class="col-md-12 col-12">
                        <div class="form-group">
                            <div class="controls">
                                <label>Shop Name</label>
                                <div class="position-relative has-icon-left">
                                  <input type="text" class="form-control" placeholder="Shop Name" value="" required="" data-validation-required-message="This username field is required">
                                  <div class="form-control-position">
                                    {{-- <i class="bx bx-user-pin"></i> --}}
                                    <i class='bx bx-info-square' ></i>
                                  </div>
                                </div>                                
                            <div class="help-block"></div></div>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="form-group">
                            <div class="controls">
                                <label>Shop Description</label>
                                <div class="position-relative has-icon-left">
                                  <textarea class="form-control" id="basicTextarea" rows="3" placeholder="Shop Description"></textarea>
                                  <div class="form-control-position">
                                    <i class="bx bx-info-circle"></i>
                                  </div>
                                </div>                                
                            <div class="help-block"></div></div>
                        </div>
                    </div>   

                  
                    <div class="col-md-6 col-12">
                        <div class="form-group">
                            <div class="controls">
                                <label>Location</label>
                                <div class="position-relative has-icon-left">
                                  <input type="text" class="form-control" placeholder="Location" value="" required="" data-validation-required-message="This username field is required">
                                  <div class="form-control-position">
                                    <i class='bx bx-map' ></i>
                                  </div>
                                </div>                                
                            <div class="help-block"></div></div>
                        </div>
                    </div>
                  
                    <div class="col-md-6 col-12">
                        <div class="form-group">
                            <div class="controls">
                                <label>Shop Website</label>
                                <div class="position-relative has-icon-left">
                                  <input type="text" class="form-control" placeholder="Shop Website" value="" required="" data-validation-required-message="This username field is required">
                                  <div class="form-control-position">
                                    <i class='bx bx-globe' ></i>
                                  </div>
                                </div>                                
                            <div class="help-block"></div></div>
                        </div>
                    </div>

                    <div class="col-md-12 col-12 mt-2">
                      <h6 >Shop contact information</h6>
                    </div>

                    <div class="col-md-6 col-12">
                        <div class="form-group">
                            <div class="controls">
                                <label>E-Mail Address</label>
                                <div class="position-relative has-icon-left">
                                  <input type="text" class="form-control" placeholder="E-Mail Address" value="{{$user->phone_number}}" required="" data-validation-required-message="This username field is required">
                                  <div class="form-control-position">
                                    <i class="bx bx-envelope"></i>
                                  </div>
                                </div>                                
                            <div class="help-block"></div></div>
                        </div>
                    </div>

                  
                    <div class="col-md-6 col-12">
                        <div class="form-group">
                            <div class="controls">
                                <label>Phone Number</label>
                                <div class="position-relative has-icon-left">
                                  <input type="text" class="form-control" placeholder="Phone Number" value="{{$user->phone_number}}" required="" data-validation-required-message="This username field is required">
                                  <div class="form-control-position">
                                    <i class="bx bx-phone"></i>
                                  </div>
                                </div>                                
                            <div class="help-block"></div></div>
                        </div>
                    </div>

                    <div class="col-md-12 col-12 mt-2">
                      <h6 >Shop Social accounts</h6>
                    </div>


                    <div class="col-md-6 col-12">
                        <div class="form-group">
                            <div class="controls">
                                <label>Facebook</label>
                                <div class="position-relative has-icon-left">
                                  <input type="text" class="form-control" placeholder="Facebook URL" value="" required="" data-validation-required-message="This username field is required">
                                  <div class="form-control-position">
                                    <i class='bx bxl-facebook-circle' ></i>
                                  </div>
                                </div>                                
                            <div class="help-block"></div></div>
                        </div>
                    </div>


                    <div class="col-md-6 col-12">
                        <div class="form-group">
                            <div class="controls">
                                <label>Twitter</label>
                                <div class="position-relative has-icon-left">
                                  <input type="text" class="form-control" placeholder="Twitter URL" value="" required="" data-validation-required-message="This username field is required">
                                  <div class="form-control-position">
                                    <i class='bx bxl-twitter' ></i>
                                  </div>
                                </div>                                
                            <div class="help-block"></div></div>
                        </div>
                    </div>

                  
                    <div class="col-md-6 col-12">
                        <div class="form-group">
                            <div class="controls">
                                <label>Instagram</label>
                                <div class="position-relative has-icon-left">
                                  <input type="text" class="form-control" placeholder="Instagram URL" value="" required="" data-validation-required-message="This username field is required">
                                  <div class="form-control-position">
                                    <i class='bx bxl-instagram-alt' ></i>
                                  </div>
                                </div>                                
                            <div class="help-block"></div></div>
                        </div>
                    </div>


                    <div class="col-md-6 col-12">
                        <div class="form-group">
                            <div class="controls">
                                <label>Youtube</label>
                                <div class="position-relative has-icon-left">
                                  <input type="text" class="form-control" placeholder="Youtube URL" value="" required="" data-validation-required-message="This username field is required">
                                  <div class="form-control-position">
                                    <i class='bx bxl-youtube' ></i>
                                  </div>
                                </div>                                
                            <div class="help-block"></div></div>
                        </div>
                    </div>

                  
               
                    <div class="col-12 d-flex flex-sm-row flex-column justify-content-end">

                        <button type="button" class="btn btn-light-secondary">Save changes</button>
                    </div>
                </div>
            </form>


          </div>
          {{-- End Account Information --}}


        </div> {{-- End Shop Information --}}


        {{-- Start Account Settings --}}
        <div class="tab-pane" id="account_settings" role="tabpanel" aria-labelledby="stacked-pill-2" aria-expanded="false">

          <form action="{{route('settings.account_settings')}}" method="POST">

            @csrf
            <input type="hidden" name="user_id" value="{{$user->id}}">
            <div class="card-body">

                <h5 class="mb-1">
                  <i class="bx bx-cog mr-25"></i>
                  Account Settings
                </h5>



                <div class="row">

                  <div class="col-6">

                      <h6 class="m-1">Actions Settings</h6>

                      <div class="col-12 mb-1">
                          <div class="custom-control custom-switch custom-switch-secondary custom-control-inline">
                              <input 
                              type="checkbox" 
                              name="can_user_create_shop[]" 
                              class="custom-control-input" 
                              id="action_can_create_own_shop" 
                              {{$user->settings->can_user_create_shop === 1 ? 'checked' : ''}}
                              value="{{$user->settings->can_user_create_shop}}" 
                              >
                              <label class="custom-control-label" for="action_can_create_own_shop">
                                <span class="switch-icon-left"><i class="bx bx-check"></i></span>
                                <span class="switch-icon-right"><i class="bx bx-x"></i></span>
                              </label>                      
                              <span class="switch-label w-100 ml-1">Can this user create <strong>Shop</strong>?</span>
                          </div>
                      </div>
                      <div class="col-12 mb-1">
                          <div class="custom-control custom-switch custom-switch-secondary custom-control-inline">
                              <input 
                              type="checkbox" 
                              class="custom-control-input" 
                              name="can_user_sell[]" 
                              id="action_can_sell_items" 
                              {{$user->settings->can_user_sell === 1 ? 'checked' : ''}}
                              value="{{$user->settings->can_user_sell}}">

                              <label class="custom-control-label" for="action_can_sell_items">
                                <span class="switch-icon-left"><i class="bx bx-check"></i></span>
                                <span class="switch-icon-right"><i class="bx bx-x"></i></span>
                              </label>                      
                              <span class="switch-label w-100 ml-1">Can this user <strong>Sell</strong> Items?</span>
                          </div>
                      </div>

                      <div class="col-12 mb-1">
                          <div class="custom-control custom-switch custom-switch-secondary custom-control-inline">
                              <input 
                              type="checkbox" 
                              class="custom-control-input" 
                              name="can_user_buy[]" 
                              id="action_can_buy_items" 
                              {{$user->settings->can_user_buy === 1 ? 'checked' : ''}}
                              value="{{$user->settings->can_user_buy}}">
                              <label class="custom-control-label" for="action_can_buy_items">
                                <span class="switch-icon-left"><i class="bx bx-check"></i></span>
                                <span class="switch-icon-right"><i class="bx bx-x"></i></span>
                              </label>                      
                              <span class="switch-label w-100 ml-1">Can this user <strong>Buy</strong> Items?</span>
                          </div>
                      </div>

                      <div class="col-12 mb-1">
                          <div class="custom-control custom-switch custom-switch-secondary custom-control-inline">
                              <input 
                              type="checkbox" 
                              class="custom-control-input" 
                              name="can_user_comment[]" 
                              id="action_can_write_comment" 
                              {{$user->settings->can_user_comment === 1 ? 'checked' : ''}}
                              value="{{$user->settings->can_user_comment}}">
                              <label class="custom-control-label" for="action_can_write_comment">
                                <span class="switch-icon-left"><i class="bx bx-check"></i></span>
                                <span class="switch-icon-right"><i class="bx bx-x"></i></span>
                              </label>                      
                              <span class="switch-label w-100 ml-1">Can this user write <strong>Comments</strong>?</span>
                          </div>
                      </div>

                      <div class="col-12 mb-1">
                          <div class="custom-control custom-switch custom-switch-secondary custom-control-inline">
                              <input 
                              type="checkbox" 
                              name="can_user_review[]" 
                              class="custom-control-input" 
                              id="action_can_create_reviews" 
                              {{$user->settings->can_user_review === 1 ? 'checked' : ''}}
                              value="{{$user->settings->can_user_review}}">
                              <label class="custom-control-label" for="action_can_create_reviews">
                                <span class="switch-icon-left"><i class="bx bx-check"></i></span>
                                <span class="switch-icon-right"><i class="bx bx-x"></i></span>
                              </label>                      
                              <span class="switch-label w-100 ml-1">Can this user create <strong>Reviews</strong> about other sellers?</span>
                          </div>
                      </div>

                  </div> {{-- End of Email SEttings --}}

                  {{-- Start Role Setting --}}
                  <div class="col-6">
                    <h6 class="m-1">Role</h6>
                      <select class="form-control ml-1" id="basicSelect">
                      <option value="User" {{$user->hasRole('User') ? 'selected' : ''}}>User</option>
                      <option value="Editor" {{$user->hasRole('Editor') ? 'selected' : ''}}>Editor</option>
                      <option value="Admin" {{$user->hasRole('Admin') ? 'selected' : ''}}>Admin</option>
                    </select>                  

                    <h6 class="m-1 mt-2">General Settings</h6>

                    <div class="col-12 mb-1">
                      <div class="checkbox">
                          <input 
                          type="checkbox"
                          name="is_verified[]" 
                          id="verification_badge_setting" 
                          {{$user->is_verified === 1 ? 'checked' : ''}}
                          value="{{$user->is_verified}}">
                          <label for="verification_badge_setting" style="cursor: pointer;">Has verification bade</label>
                      </div>         
                    </div>

                    <div class="col-12 mb-1">
                      <div class="checkbox">
                          <input 
                          type="checkbox" 
                          name="email_verified_at[]" 
                          id="email_verified_setting" 
                          {{$user->email_verified_at === null ? '' : 'checked'}}
                          value="{{$user->email_verified_at}}">
                          <label for="email_verified_setting" style="cursor: pointer;">E-Mail verified</label>
                      </div>         
                    </div>

                    <div class="col-12 mb-1">
                      <div class="checkbox">
                          <input 
                          type="checkbox"
                          name="can_user_contact_support_team[]" 
                          id="can_contact_supprt_settings" 
                          {{$user->settings->can_user_contact_support_team === 1 ? 'checked' : ''}}
                          value="{{$user->settings->can_user_contact_support_team}}">
                          <label for="can_contact_supprt_settings" style="cursor: pointer;">Can contact Supprt-Team</label>
                      </div>         
                    </div>

                  </div> {{-- End Role Setting --}}


                  {{-- Start Privacy Setting --}}
                  <div class="col-6">

                      <h6 class="m-1">Privacy Settings</h6>

                      <div class="col-12 mb-1">
                          <div class="custom-control custom-switch custom-switch-secondary custom-control-inline">
                              <input 
                              type="checkbox" 
                              name="can_visitor_show_user_shop[]" 
                              class="custom-control-input" 
                              id="privacy_show_profile_settings" 
                              {{$user->settings->can_visitor_show_user_shop === 1 ? 'checked' : ''}}
                              value="{{$user->settings->can_visitor_show_user_shop}}">
                              <label class="custom-control-label" for="privacy_show_profile_settings">
                                <span class="switch-icon-left"><i class="bx bx-check"></i></span>
                                <span class="switch-icon-right"><i class="bx bx-x"></i></span>
                              </label>                      
                              <span class="switch-label w-100 ml-1">Show my Profile just when <strong>Vistor</strong> Flooma user?</span>
                          </div>
                      </div>
                  </div> {{-- End of Privacy SEttings --}}



                  {{-- Start Account Status Setting --}}
                  <div class="col-6">

                    <h6 class="m-1 mt-2">Account Status</h6>

                    <ul class="list-unstyled mb-0 ml-1">

                      <li class="d-inline-block mr-2">
                        <fieldset>
                          <div class="radio radio-sm">
                              <input type="radio" 
                              id="radiosmall" 
                              name="is_active[]" 
                              {{$user->is_active === 1 ? 'checked' : ''}}
                              value="{{$user->is_active}}">
                              <label for="radiosmall">Active</label>
                          </div>
                        </fieldset>
                      </li>

                      <li class="d-inline-block mr-2">
                        <fieldset>
                          <div class="radio radio-sm">
                              <input type="radio" 
                              id="radiosmall1" 
                              name="is_active[]" 
                              {{$user->is_active === 2 ? 'checked' : ''}}
                              value="{{$user->is_active}}">
                              <label for="radiosmall1">Pendding</label>
                          </div>
                        </fieldset>
                      </li>

                      <li class="d-inline-block mr-2">
                        <fieldset>
                          <div class="radio radio-sm">
                              <input type="radio" 
                              id="radiosmall2" 
                              name="is_active[]" 
                              {{$user->is_active === 0 ? 'checked' : ''}}
                              value="{{$user->is_active}}">
                              <label for="radiosmall2">In active</label>
                          </div>
                        </fieldset>
                      </li>


                    </ul>

                  </div> {{-- End of Account Status SEttings --}}



                </div>

                <div class="col-12 d-flex flex-sm-row flex-column justify-content-end">
                  <button type="submit" class="btn btn-light-secondary">Save changes</button>
                </div>              

            </div>

          </form>


        </div> {{-- End Account Settings --}}


        <div class="tab-pane" id="preferences" role="tabpanel" aria-labelledby="stacked-pill-2" aria-expanded="false">


          <div class="card-body">

              <h5 class="mb-1">
                <i class="bx bx-ball mr-25"></i>
                Preferences
              </h5>



              <div class="row">

                <div class="col-6">

                    <h6 class="m-1">Account Preferences</h6>

                    <div class="col-12 mb-1">
                        <div class="custom-control custom-switch custom-switch-secondary custom-control-inline">
                            <input type="checkbox" class="custom-control-input" checked id="customSwitch10">
                            <label class="custom-control-label mr-1" for="customSwitch10">
                              <span class="switch-icon-left"><i class="bx bx-check"></i></span>
                              <span class="switch-icon-right"><i class="bx bx-x"></i></span>
                            </label>                      
                            <span class="switch-label w-100">Send email about new product?</span>
                        </div>
                    </div>
               
                    <div class="col-12 mb-1">
                        <div class="custom-control custom-switch custom-switch-secondary custom-control-inline">
                            <input type="checkbox" class="custom-control-input" checked id="customSwitch10">
                            <label class="custom-control-label mr-1" for="customSwitch10">
                              <span class="switch-icon-left"><i class="bx bx-check"></i></span>
                              <span class="switch-icon-right"><i class="bx bx-x"></i></span>
                            </label>                      
                            <span class="switch-label w-100">Send email when user has a new offer?</span>
                        </div>
                    </div>
               
                    <div class="col-12 mb-1">
                        <div class="custom-control custom-switch custom-switch-secondary custom-control-inline">
                            <input type="checkbox" class="custom-control-input" checked id="customSwitch10">
                            <label class="custom-control-label mr-1" for="customSwitch10">
                              <span class="switch-icon-left"><i class="bx bx-check"></i></span>
                              <span class="switch-icon-right"><i class="bx bx-x"></i></span>
                            </label>                      
                            <span class="switch-label w-100">Send email when user has new comment?</span>
                        </div>
                    </div>
               
                    <div class="col-12 mb-1">
                        <div class="custom-control custom-switch custom-switch-secondary custom-control-inline">
                            <input type="checkbox" class="custom-control-input" checked id="customSwitch10">
                            <label class="custom-control-label mr-1" for="customSwitch10">
                              <span class="switch-icon-left"><i class="bx bx-check"></i></span>
                              <span class="switch-icon-right"><i class="bx bx-x"></i></span>
                            </label>                      
                            <span class="switch-label w-100">Send email when user has new review?</span>
                        </div>
                    </div>
               
                    <div class="col-12 mb-1">
                        <div class="custom-control custom-switch custom-switch-secondary custom-control-inline">
                            <input type="checkbox" class="custom-control-input" checked id="customSwitch10">
                            <label class="custom-control-label mr-1" for="customSwitch10">
                              <span class="switch-icon-left"><i class="bx bx-check"></i></span>
                              <span class="switch-icon-right"><i class="bx bx-x"></i></span>
                            </label>                      
                            <span class="switch-label w-100">Send email when user has new message?</span>
                        </div>
                    </div>

                  </div> 

                  <div class="col-6">

                      <h6 class="m-1">Shop Preferences</h6>

                        <div class="col-12 mb-1">
                            <div class="custom-control custom-switch custom-switch-secondary custom-control-inline">
                                <input type="checkbox" class="custom-control-input" checked id="customSwitch10">
                                <label class="custom-control-label mr-1" for="customSwitch10">
                                  <span class="switch-icon-left"><i class="bx bx-check"></i></span>
                                  <span class="switch-icon-right"><i class="bx bx-x"></i></span>
                                </label>                      
                                <span class="switch-label w-100">Send email about new product?</span>
                            </div>
                        </div>
                   
                        <div class="col-12 mb-1">
                            <div class="custom-control custom-switch custom-switch-secondary custom-control-inline">
                                <input type="checkbox" class="custom-control-input" checked id="customSwitch10">
                                <label class="custom-control-label mr-1" for="customSwitch10">
                                  <span class="switch-icon-left"><i class="bx bx-check"></i></span>
                                  <span class="switch-icon-right"><i class="bx bx-x"></i></span>
                                </label>                      
                                <span class="switch-label w-100">Send email when user has a new offer?</span>
                            </div>
                        </div>
                   
                        <div class="col-12 mb-1">
                            <div class="custom-control custom-switch custom-switch-secondary custom-control-inline">
                                <input type="checkbox" class="custom-control-input" checked id="customSwitch10">
                                <label class="custom-control-label mr-1" for="customSwitch10">
                                  <span class="switch-icon-left"><i class="bx bx-check"></i></span>
                                  <span class="switch-icon-right"><i class="bx bx-x"></i></span>
                                </label>                      
                                <span class="switch-label w-100">Send email when user has new comment?</span>
                            </div>
                        </div>
                   
                        <div class="col-12 mb-1">
                            <div class="custom-control custom-switch custom-switch-secondary custom-control-inline">
                                <input type="checkbox" class="custom-control-input" checked id="customSwitch10">
                                <label class="custom-control-label mr-1" for="customSwitch10">
                                  <span class="switch-icon-left"><i class="bx bx-check"></i></span>
                                  <span class="switch-icon-right"><i class="bx bx-x"></i></span>
                                </label>                      
                                <span class="switch-label w-100">Send email when user has new review?</span>
                            </div>
                        </div>
                   
                        <div class="col-12 mb-1">
                            <div class="custom-control custom-switch custom-switch-secondary custom-control-inline">
                                <input type="checkbox" class="custom-control-input" checked id="customSwitch10">
                                <label class="custom-control-label mr-1" for="customSwitch10">
                                  <span class="switch-icon-left"><i class="bx bx-check"></i></span>
                                  <span class="switch-icon-right"><i class="bx bx-x"></i></span>
                                </label>                      
                                <span class="switch-label w-100">Send email when user has new message?</span>
                            </div>
                        </div>

                  </div> 


              </div>

              <div class="col-12 d-flex flex-sm-row flex-column justify-content-end">
                <button type="button" class="btn btn-light-secondary">Save</button>
              </div>              

          </div>
        </div>


        <div class="tab-pane" id="notifications" role="tabpanel" aria-labelledby="stacked-pill-3" aria-expanded="false">
          Notifications
        </div>


      </div>
    </div>
  </div>
</div>
@stop





{{-- Start Js Vendors --}}
@section('vendor-js')
<script src="{{asset('static/backend/app-assets/vendors/js/forms/validation/jqBootstrapValidation.js')}}"></script>
@stop{{-- End Js Vendors --}}

{{-- Start Js pages --}}
@section('pages-js')
<script src="{{asset('static/backend/app-assets/js/scripts/pages/page-account-settings.min.js')}}"></script>
@stop{{-- End Js pages --}}
