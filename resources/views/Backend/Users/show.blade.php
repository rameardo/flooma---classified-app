{{-- Extends The Root --}}
@extends('backend.root')

{{-- Start Css Vendors --}}
@section('vendor-css')

@stop {{-- End Css Vendors --}}


{{-- Start Pages Ttitle --}}

@section('pages-title')
Dashboard
@stop

{{-- End Pages Ttitle --}}





{{-- Start Page Content --}}
@section('content')




<section class="page-user-profile">
  <div class="row">
    <div class="col-12">


      <!-- user profile content section start -->
      <div class="row">
        <!-- user profile nav tabs content start -->
        <div class="col-lg-9">
          <div class="s_">

            <div class="tab-pane" id="profile" aria-labelledby="profile-tab" role="tabpanel">
              <!-- user profile nav tabs profile start -->
              <div class="card">
                <div class="card-content">
                  <div class="card-body">
                    <div class="row">
                      <div class="col-12">
                        <div class="row">
                          <div class="col-12 col-sm-3 text-center mb-1 mb-sm-0">

                            @if ($user->profile_image === null)

                                <img style="border-radius: 4px;" src="{{asset('static/public/user/default.png')}}" alt="{{$user->first_name}} {{$user->last_name}} image" height="140" width="120">

                            @else

                            <img style="border-radius: 4px;" src="{{asset('static/public/user/')}} {{$user->profile_image}}" alt="{{$user->first_name}} {{$user->last_name}} image" height="150" width="120">

                            @endif


                          </div>
                          <div class="col-12 col-sm-9">
                            <div class="row">
                              <div class="col-12 text-center text-sm-left">
                                <h6 class="media-heading mb-0">
                                  <span class="d-flex align-items-center">
                                    {{$user->first_name}} {{$user->last_name}}

                                    @if ($user->isOnline())
                                      <span class="bullet bullet-success bullet-sm user-status-show-backend cursor-pointer"
                                      data-toggle="tooltip"
                                      data-placement="bottom" 
                                      title="Currently Online"></span>
                                    @else
                                      <span class="bullet bullet-secondary bullet-sm user-status-show-backend cursor-pointer"
                                      data-toggle="tooltip"
                                      data-placement="bottom" 
                                      title="Currently Offline"></span>
                                    @endif

                                  </span>
                                    

                                    <a href="#" target="_blank" class="btn btn-sm d-none d-sm-block float-right btn-light-secondary">
                                      <span>Visit this Profile</span>
                                    </a>

                                    <a href="#" class="btn btn-sm d-block d-sm-none btn-block text-center btn-light-secondary">
                                      <i class="cursor-pointer bx bx-show-alt font-small-3 mr-25"></i><span>Visit this Profile</span>
                                    </a>        

                                </h6>
                                <small class="text-muted align-top">
                                    {{'@' . $user->name}}
                                </small>
                              </div>
                              <div class="col-12 text-center text-sm-left">
                                <div class="mb-1">

                                  <span class="mr-1">122 <small>Products</small></span>

                                  <span class="mr-1">4.7k <small>Offers</small></span>

                                  <span class="mr-1">652 <small>Comments</small></span>

                                </div>
                                <p>
                                    There are no Notes from Administrators on {{$user->first_name}} {{$user->last_name}} 👍 
                                </p>
                                <div>

                                @if ($user->settings->own_store == 1)
                                  <div class="badge badge-light-secondary badge-round mr-1 mb-1 cursor-pointer" data-toggle="tooltip"
                                    data-placement="bottom" title="Has his own store"><i
                                      class="bx bx-strikethrough"></i>
                                  </div>   
                                @endif

    

                                @if ($user->settings->membership_type == 1)
                                  <div class="badge badge-light-secondary badge-round mr-1 mb-1 cursor-pointer" data-toggle="tooltip"
                                    data-placement="bottom" title="Premium membership"><i
                                      class="bx bx-diamond"></i>
                                  </div>    
                                @endif

    

   

                                  <div class="badge badge-light-primary badge-round mr-1 mb-1 cursor-pointer" data-toggle="tooltip"
                                    data-placement="bottom" title=" Verified E-Mail"><i
                                      class="bx bx-check-shield"></i>
                                  </div>

                                @if ($user->is_verified == 1)
                                  <div class="badge badge-light-warning badge-round mr-1 mb-1 cursor-pointer" data-toggle="tooltip"
                                    data-placement="bottom" title="Has verification badge"><i
                                      class="bx bx-badge-check"></i>
                                  </div>
                                @endif                             


                                </div>

                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card">
                <div class="card-content">
                  <div class="card-body">
                    <h5 class="card-title">
                        Basic Informations

                    <a href="{{route('admin.users.index')}}/{{$user->id}}/edit" class="btn btn-sm d-none d-sm-block float-right btn-light-secondary mb-2">
                      <i class="cursor-pointer bx bx-edit-alt font-small-3 mr-50"></i><span>Edit</span>
                    </a href="#">
                    <a href="#" class="btn btn-sm d-block d-sm-none btn-block text-center btn-light-secondary">
                      <i class="cursor-pointer bx bx-edit-alt font-small-3 mr-25"></i><span>Edit</span>
                    </a>        

                    </h5>

                    <div class="row">                      
                      <div class="col-6">
                        <h6>
                            <small class="text-muted d-flex align-items-center">
                                <i class="bx bx-user-pin profile-show-icon mr-25"></i>
                                First Name
                            </small>
                        </h6>
                        <p class="ml-2">
                            @if ($user->first_name === null)
                                N/A
                            @else
                                {{$user->first_name}}
                            @endif                                
                        </p>
                      </div>

                       <div class="col-6">
                        <h6>
                            <small class="text-muted d-flex align-items-center">
                                <i class="bx bx-user-pin profile-show-icon mr-25"></i>
                                Last Name
                            </small>
                        </h6>
                        <p class="ml-2">
                            @if ($user->last_name === null)
                                N/A
                            @else
                                {{$user->last_name}}
                            @endif                                
                        </p>
                      </div>

                       <div class="col-6">
                        <h6>
                            <small class="text-muted d-flex align-items-center">
                                <i class="bx bx-user profile-show-icon mr-25"></i>
                                Username
                            </small>
                        </h6>
                        <p class="ml-2">
                            @if ($user->name === null)
                                N/A
                            @else
                                {{$user->name}}
                            @endif                               
                        </p>
                      </div>

                       <div class="col-6">
                        <h6>
                            <small class="text-muted d-flex align-items-center">
                                <i class="bx bx-envelope profile-show-icon mr-25"></i>
                                E-Mail Address
                            </small>
                        </h6>
                        <p class="ml-2">{{$user->email}}</p>
                      </div>

                       <div class="col-6">
                        <h6>
                            <small class="text-muted d-flex align-items-center">
                                <i class="bx bx-phone profile-show-icon mr-25"></i>
                                Phone Number
                            </small>
                        </h6>
                        <p class="ml-2">
                            @if ($user->phone_number === null)
                                N/A
                            @else
                                {{$user->phone_number}}
                            @endif                              
                        </p>
                      </div>

                       <div class="col-6">
                        <h6>
                            <small class="text-muted d-flex align-items-center">
                                <i class="bx bx-planet profile-show-icon mr-25"></i>
                                IP Address
                            </small>
                        </h6>
                        <p class="ml-2">
                            @if ($user->ip_address === null)
                                N/A
                            @else
                                {{$user->ip_address}}
                            @endif                            
                        </p>
                      </div>

                       <div class="col-6">
                        <h6>
                            <small class="text-muted d-flex align-items-center">
                                <i class="bx bx-check profile-show-icon mr-25"></i>
                                E-Mail Verified
                            </small>
                        </h6>
                        <p class="ml-2">
                            @if ($user->email_verified_at === null)
                                Not verified
                            @else
                                Verified
                            @endif                            
                        </p>
                      </div>

                       <div class="col-6">
                        <h6>
                            <small class="text-muted d-flex align-items-center">
                                <i class="bx bx-landscape profile-show-icon mr-25"></i>
                                Account status
                            </small>
                        </h6>
                        <p class="ml-2">
                            @if ($user->is_active == 0)
                              Not active
                            @else
                              Active
                            @endif
                        </p>
                      </div>

                       <div class="col-6">
                        <h6>
                            <small class="text-muted d-flex align-items-center">
                                <i class="bx bx-time profile-show-icon mr-25"></i>
                                Join Date
                            </small>
                        </h6>
                        <p class="ml-2">{{$user->created_at->diffForHumans()}}</p>
                      </div>

                       <div class="col-6">
                        <h6>
                            <small class="text-muted d-flex align-items-center">
                                <i class="bx bx-time-five profile-show-icon mr-25"></i>
                                Last account update
                            </small>
                        </h6>
                        <p class="ml-2">{{$user->updated_at->diffForHumans()}}</p>
                      </div>

            

                    </div>

                  </div>
                </div>
              </div>
              <!-- user profile nav tabs profile ends -->
            </div>
          </div>

          <div class="s_">

            <div class="tab-pane" id="profile" aria-labelledby="profile-tab" role="tabpanel">
              <!-- user profile nav tabs profile start -->

              <div class="card">
                <div class="card-content">
                  <div class="card-body">
                    <h5 class="card-title">
                        {{$user->first_name}} {{$user->last_name}} activity


                    @if (!$activity_count == 0 || !$activity_count < 1)
                      <a href="{{route('user.activity.backend', $user->id)}}" class="btn btn-sm d-none d-sm-block float-right btn-light-secondary mb-2">
                        <i class="cursor-pointer bx bx-show-alt font-small-3 mr-50"></i><span>View all</span>
                      </a>
                      <a href="{{route('user.activity.backend', $user->id)}}" class="btn btn-sm d-block d-sm-none btn-block text-center btn-light-secondary">
                        <i class="cursor-pointer bx bx-show-alt font-small-3 mr-25"></i><span>View all</span>
                      </a>      
                    @endif
  

                    </h5>

                    {{-- Handle the Activity table --}}
                    @if ($activity_count == 0 || $activity_count < 1)
                      <div class="no-record-sub">
                        <img src="{{asset('static/backend/assets/images/no/no-records.png')}}">
                        <h4>{{$user->first_name}} {{$user->last_name}} has no activities yet!</h4>
                      </div>
                    @endif

                    <ul class="widget-timeline mb-0">
                      @foreach ($activities as $activity)
                
                                              
                        <li class="timeline-items timeline-icon-secondary active">
                          @if ($activity->created_at !== null)
                            <div class="timeline-time">{{$activity->created_at->format('F, j ')}}</div>
                          @endif
                          
                          <h6 class="timeline-title">
                            
                            @if ($activity->signal == 'comment')
                              <a href="{{$activity->first_value_link}}">{{$activity->first_value}}</a>
                               reacted to
                              <a href="{{$activity->second_value_link}}">{{$activity->second_value}}</a>s <a href="{{$activity->activity_url}}">Post</a>
                            @endif

                            @if ($activity->signal == 'join')
                              <a href="#" target="_blank">{{$activity->first_value}}</a> joined Flooma
                            @endif

                          </h6>

                          @if ($activity->created_at !== null)
                            <p class="timeline-text">{{$activity->created_at->diffForHumans()}}</p>
                          @endif      

                          @if ($activity->signal !== null)
                            @if ($activity->signal == 'comment')
                              <div class="timeline-content">
                                <img src="{{asset('static/backend/app-assets/images/icon/pdf.png')}}" alt="document" height="23" width="19" class="mr-50">
                                  New Order.pdf
                              </div>
                            @endif

                            
                          @endif
                        </li>

                      @endforeach
      
                    </ul>  

                  </div>
                </div>
              </div>
              <!-- user profile nav tabs profile ends -->
            </div>
          </div>



        </div>
        <!-- user profile nav tabs content ends -->
        <!-- user profile right side content start -->
        <div class="col-lg-3">

          <div class="simple-statistic-backend card">
            <div class="d-flex">

              <div class="col-md-6 ssb-section">
                <p>Total Sales</p>
                <h3>51</h3>
              </div>
              
              <div class="col-md-6 ssb-section">
                <p>Total Earnings</p>
                <h3>$4.7K</h3>
              </div>

            </div>
          </div>

          <!-- user profile right side content related groups start -->
          <div class="card">
            <div class="card-content">
              <div class="card-body">
                <h6 class="card-title mb-1">
                    <small>
                        Last 3 Products from {{$user->name}}
                    </small>              
                </h6>

                <div class="media d-flex align-items-center mb-1">
                  <a href="JavaScript:void(0);">
                    <img src="{{asset('static/backend/app-assets/images/banner/banner-30.jpg')}}" class="rounded" alt="group image"
                      height="64" width="64" />
                  </a>
                  <div class="media-body ml-1">
                    <h6 class="media-heading mb-0"><small>Play Guitar</small></h6><small class="text-muted">2.8k
                      members (7 joined)</small>
                  </div>
                  <i class="cursor-pointer bx bx-show-alt text-primary d-flex align-items-center"></i>
                </div>

                <div class="media d-flex align-items-center mb-1">
                  <a href="JavaScript:void(0);">
                    <img src="{{asset('static/backend/app-assets/images/banner/banner-30.jpg')}}" class="rounded" alt="group image"
                      height="64" width="64" />
                  </a>
                  <div class="media-body ml-1">
                    <h6 class="media-heading mb-0"><small>Play Guitar</small></h6><small class="text-muted">2.8k
                      members (7 joined)</small>
                  </div>
                  <i class="cursor-pointer bx bx-show-alt text-primary d-flex align-items-center"></i>
                </div>
                
                <div class="media d-flex align-items-center mb-1">
                  <a href="JavaScript:void(0);">
                    <img src="{{asset('static/backend/app-assets/images/banner/banner-30.jpg')}}" class="rounded" alt="group image"
                      height="64" width="64" />
                  </a>
                  <div class="media-body ml-1">
                    <h6 class="media-heading mb-0"><small>Play Guitar</small></h6><small class="text-muted">2.8k
                      members (7 joined)</small>
                  </div>
                  <i class="cursor-pointer bx bx-show-alt text-primary d-flex align-items-center"></i>
                </div>

              </div>
            </div>
          </div>
          <!-- user profile right side content related groups ends -->


        </div>
        <!-- user profile right side content ends -->
      </div>
      <!-- user profile content section start -->
    </div>
  </div>
</section>

@stop





{{-- Start Js Vendors --}}
@section('vendor-js')

@stop{{-- End Js Vendors --}}

{{-- Start Js pages --}}
@section('pages-js')

<script src="{{asset('static/backend/app-assets/js/scripts/modal/components-modal.min.js')}}"></script>
<script src="{{asset('static/backend/app-assets/js/scripts/popover/popover.min.js')}}"></script>
@stop{{-- End Js pages --}}
