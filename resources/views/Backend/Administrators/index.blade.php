{{-- Extends The Root --}}
@extends('backend.root')

{{-- Start Css Vendors --}}
@section('vendor-css')
<link rel="stylesheet" type="text/css" href="{{asset('static/backend/app-assets/vendors/css/tables/datatable/datatables.min.css')}}">
@stop {{-- End Css Vendors --}}


{{-- Start Pages Ttitle --}}

@section('pages-title')
Dashboard
@stop

{{-- End Pages Ttitle --}}





{{-- Start Page Content --}}
@section('content')

@if ($users_counter == 0)


 <section class="empty-records">
     <img class="no-data-image" src="{{asset('static/backend/assets/images/errors/no_data.svg')}}">
     <div class="no-data-msg">There are no users yet!</div>
 </section>


@else

<!-- Zero configuration table -->
<section id="column-selectors">
    <div class="row">
        <div class="col-12">
            <div class="card flooma-card-table">
                <div class="card-header d-flex justify-content-between ">
                    <h4 class="font-large-1">
                        
                        Administrators

                    </h4>
										<a href="#" class="btn btn-light-secondary glow mr-1 mb-1">
											<i class='bx bx-plus'></i>
											New admin
                		</a>
                </div>
                <div class="card-content">
                    <div class="card-body card-dashboard">

                        <div class="table-responsive flooma-table">
                            <table class="table table-striped dataex-html5-selectors">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Username</th>
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>E-Mail Address</th>
                                        <th>Role</th>
                                        <th>Register date</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                	@foreach($users as $user)
                                    <tr>
                                    	<td>{{$user->id}}</td>
 
                                        <td class="py-1 line-ellipsis">

                                            @if ($user->profile_image === null)

                                                <img class="rounded-circle mr-1" src="{{asset('static/public/user/default.png')}}" alt="card" height="24" width="24">

                                            @else

                                            <img class="rounded-circle mr-1" src="{{asset('static/public/user/')}} {{$user->profile_image}}" alt="card" height="24" width="24">

                                            @endif
                                          

                                          {{$user->name}}
                                        </td>     

                                        <td>
                                            @if ($user->first_name === null)
                                            <span style="cursor: pointer;" 
                                            class="tooltip-light" 
                                            data-toggle="tooltip" 
                                            data-placement="top"
                                            data-original-title="Not available">
                                                N/A
                                            </span>                                            
                                                
                                            @else
                                               {{$user->first_name}}
                                            @endif
                                        </td>
 

                                        <td>
                                            @if ($user->last_name === null)
                                            <span style="cursor: pointer;" 
                                            data-toggle="tooltip" 
                                            data-placement="top" title="" 
                                            data-original-title="Not available">
                                                N/A
                                            </span>                                            
                                                
                                            @else
                                               {{$user->last_name}}
                                            @endif
                                        </td>
 

                                        <td>
                                            {{$user->email}}
                                            @if ($user->email_verified_at === null)
                                                <i class='bx bx-x-circle' 
                                                style="cursor: pointer;position: absolute;margin-left: 5px;margin-top: 1px;" 
                                                data-trigger="hover"  
                                                data-toggle="popover" 
                                                data-placement="top" 
                                                data-container="body" 
                                                data-original-title="{{$user->email}}" 
                                                data-content="This E-Mail Address is Not Verified"></i>

     
                                            @else
                                                <i class='bx bx-check-circle' 
                                                style="cursor: pointer;position: absolute;margin-left: 5px;margin-top: 1px;" 
                                                data-trigger="hover"  
                                                data-toggle="popover" 
                                                data-placement="top" 
                                                data-container="body" 
                                                data-original-title="{{$user->email}}" 
                                                data-content="This E-Mail Address is Verified"></i>                                            
                                            @endif
                                            


                                        </td>
 {{--                                        <td>Edinburgh</td>
                                        <td class="text-success py-1">Active</td> --}}

                                        <td>
                                                
                                            @if ($user->hasRole('Admin'))
                                                <div class="badge badge-light-secondary">Admin</div>

                                            @elseif ($user->hasRole('Editor'))
                                                <div class="badge badge-light-secondary ">Editor</div>

                                            @endif

                                        </td>

                                        <td>
                                            <span style="cursor: pointer;" 
                                            data-toggle="tooltip" 
                                            data-placement="top" title="" 
                                            data-original-title="{{$user->created_at->format('d.m.Y')}}">
                                                {{$user->created_at->diffForHumans()}}
                                            </span>                                             
                                        </td>

                                        <td>


                                            <a href="{{route('admin.users.index')}}/{{$user->id}}" 
                                            class="btn flooma-btn btn-icon btn-light-secondary"
                                            data-toggle="tooltip" 
                                            data-placement="top" title="" 
                                            data-original-title="View">
                                                <i class='bx bx-show-alt'></i>
                                            </a>

                                            <a href="{{route('admin.users.index')}}/{{$user->id}}/edit" 
                                            class="btn flooma-btn btn-icon btn-light-secondary"
                                            data-toggle="tooltip" 
                                            data-placement="top" title="" 
                                            data-original-title="Edit">
                                                <i class='bx bx-edit-alt'></i>
                                            </a>

                       
                      
                                        	<a href="{{$user->id}}" class="btn flooma-btn btn-icon btn-light-secondary" data-toggle="modal" data-target="#confirmationDelete">
                                        		<i class='bx bx-trash' ></i>
                                        	</a>
                                        	
                                        </td>
                                    </tr>

    								<div class="modal fade" id="confirmationDelete" tabindex="-1" role="dialog"
    								  aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    								  <div class="modal-dialog modal-dialog-centered modal-dialog-centered modal-dialog-scrollable"
    								    role="document">
    								    <div class="modal-content">
    								      <div class="modal-header">
    								        <h5 class="modal-title" id="exampleModalCenterTitle">Delete user</h5>
    								        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    								          <i class="bx bx-x"></i>
    								        </button>
    								      </div>
    								      <div class="modal-body">
    								        <p>
    								          Are you sure you want to delete this {{$user->id
                                              }} ?
    								        </p>
    								      </div>
    								      <div class="modal-footer">
    								        <button type="button" class="btn btn-light-secondary" data-dismiss="modal">
    								          <i class="bx bx-x d-block d-sm-none"></i>
    								          <span class="d-none d-sm-block">No</span>
    								        </button>
    								        <button type="button" class="btn btn-secondary ml-1" data-dismiss="modal">
    								          <i class="bx bx-check d-block d-sm-none"></i>
    								          <span class="d-none d-sm-block">Yes</span>
    								        </button>
    								      </div>
    								    </div>
    								  </div>
    								</div>

                                   @endforeach
                                </tbody>

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--/ Zero configuration table -->

@endif {{-- End if of Counter --}}


@stop





{{-- Start Js Vendors --}}
@section('vendor-js')
<script src="{{asset('static/backend/app-assets/vendors/js/tables/datatable/datatables.min.js')}}"></script>
<script src="{{asset('static/backend/app-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('static/backend/app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('static/backend/app-assets/vendors/js/tables/datatable/buttons.html5.min.js')}}"></script>
<script src="{{asset('static/backend/app-assets/vendors/js/tables/datatable/buttons.print.min.js')}}"></script>
<script src="{{asset('static/backend/app-assets/vendors/js/tables/datatable/pdfmake.min.js')}}"></script>
<script src="{{asset('static/backend/app-assets/vendors/js/tables/datatable/vfs_fonts.js')}}"></script>
@stop{{-- End Js Vendors --}}

{{-- Start Js pages --}}
@section('pages-js')
<script src="{{asset('static/backend/app-assets/js/scripts/datatables/datatable.min.js')}}"></script>
<script src="{{asset('static/backend/app-assets/js/scripts/modal/components-modal.min.js')}}"></script>
<script src="{{asset('static/backend/app-assets/js/scripts/popover/popover.min.js')}}"></script>
@stop{{-- End Js pages --}}
