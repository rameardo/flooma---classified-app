{{-- Extends The Root --}}
@extends('backend.root')

{{-- Start Css Vendors --}}
@section('vendor-css')
<link rel="stylesheet" type="text/css" href="{{asset('static/backend/app-assets/vendors/css/tables/datatable/datatables.min.css')}}">
@stop {{-- End Css Vendors --}}


{{-- Start Pages Ttitle --}}

@section('pages-title')
Dashboard
@stop

{{-- End Pages Ttitle --}}





{{-- Start Page Content --}}
@section('content')



{{-- 
 <section class="empty-records">
     <img class="no-data-image" src="{{asset('static/backend/assets/images/errors/no_data.svg')}}">
     <div class="no-data-msg">There are no users yet!</div>
 </section> --}}




<!-- Zero configuration table -->
<section id="column-selectors">
    <div class="row">
        <div class="col-12">
            <div class="card flooma-card-table">
                <div class="card-header d-flex justify-content-between ">
                    <h4 class="font-large-1">{{$language->name}}</h4>
                </div>
                <div class="card-content">
                    <div class="card-body card-dashboard">

                        <div class="table-responsive flooma-table">
                            <table class="table table-striped dataex-html5-selectors">
                                <thead>
                                    <tr>
                                        <th width="5%">#</th>
                                        <th width="30%">Key</th>
                                        <th>Value</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                    @foreach ($translations as $trans)
                                        <tr>

                                            <td>{{$trans->id}}</td>
                                            <td>
                                                {{-- <input type="text" class="form-control" id="readonlyInput" readonly="readonly" value="{{$trans->key}}"> --}}
                                                <input type="text" id="readonlyInput" readonly="readonly" class="form-control square" value="{{ $trans->key}}" placeholder="{{$trans->key}}">
                                            </td>

                                            <td>
                                                <input type="text" class="form-control square" value="{{$trans->value}}" placeholder="{{$trans->value}}">
                                            </td>

                                        </tr>
                                    @endforeach


                                    
                                </tbody>
                            </table>
                            <div class="d-flex justify-content-end mt-1">
                                <a href="{{route('admin.languages.index')}}" class="btn btn-light-secondary mr-1">Back</a>
                                <a href="#" class="btn btn-light-secondary ">Save changes</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--/ Zero configuration table -->


@stop





{{-- Start Js Vendors --}}
@section('vendor-js')
<script src="{{asset('static/backend/app-assets/vendors/js/tables/datatable/datatables.min.js')}}"></script>
<script src="{{asset('static/backend/app-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('static/backend/app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('static/backend/app-assets/vendors/js/tables/datatable/buttons.html5.min.js')}}"></script>
<script src="{{asset('static/backend/app-assets/vendors/js/tables/datatable/buttons.print.min.js')}}"></script>
<script src="{{asset('static/backend/app-assets/vendors/js/tables/datatable/pdfmake.min.js')}}"></script>
<script src="{{asset('static/backend/app-assets/vendors/js/tables/datatable/vfs_fonts.js')}}"></script>
@stop{{-- End Js Vendors --}}

{{-- Start Js pages --}}
@section('pages-js')
<script src="{{asset('static/backend/app-assets/js/scripts/datatables/datatable.min.js')}}"></script>
<script src="{{asset('static/backend/app-assets/js/scripts/modal/components-modal.min.js')}}"></script>
<script src="{{asset('static/backend/app-assets/js/scripts/popover/popover.min.js')}}"></script>
@stop{{-- End Js pages --}}
