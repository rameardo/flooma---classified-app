{{-- Extends The Root --}}
@extends('backend.root')

{{-- Start Css Vendors --}}
@section('vendor-css')
<link rel="stylesheet" type="text/css" href="{{asset('static/backend/app-assets/vendors/css/tables/datatable/datatables.min.css')}}">
@stop {{-- End Css Vendors --}}


{{-- Start Pages Ttitle --}}

@section('pages-title')
Dashboard
@stop

{{-- End Pages Ttitle --}}





{{-- Start Page Content --}}
@section('content')
{{visitors('isp')}}
{{get_ip()}}
{{$default_lang->value}}
<div class="row">

  <div class="col-xl-4 col-md-6 col-sm-12">

    <div class="card">
      <div class="card-content">
        <div class="card-body">
          <h4 class="card-title">Default Website Language </h4>
          <p class="card-text">
            Here you can choose the default language for the site.
          </p>
          <form class="form" id="form" method="post" action="{{route('admin.languages.setDefaultLanguage')}}">
            @csrf
            <div class="form-body">

              <div class="form-group">

                <select name="default_lang" class="form-control">

                  @foreach ($languages as $language)
                    <option value="{{$language->shortcut}}" {{$language->shortcut === $default_lang->value ? 'selected' : ''}}>{{$language->name}}</option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="form-actions d-flex justify-content-end">
              <button type="submit" class="btn btn-light-secondary ">Save Changes</button>
            </div>
          </form>
        </div>
      </div>
    </div>

    <div class="add-new-lang">

      <a href="{{route('admin.languages.newLang')}}" class="#">
        <img src="https://image.flaticon.com/icons/svg/1373/1373419.svg" width="50" height="50" class="d-block">
        Add new Language
      </a>

    </div>

  </div>


  <div class="col-xl-8 col-12 dashboard-marketing-campaign language">
        <div class="card marketing-campaigns">
            <div class="card-header d-flex justify-content-between align-items-center pb-1">
            <h4 class="card-title">Languages</h4>
            <i class="bx bx-dots-vertical-rounded font-medium-3 cursor-pointer"></i>
          </div>

          <div class="table-responsive ps">
            <!-- table start -->
            <table id="table-marketing-campaigns" class="table table-borderless table-marketing-campaigns mb-0">
              <thead>
                <tr>
                  <th>Language</th>
                  <th>Shortcut</th>
                  <th>Dirction</th>
                  <th>Status</th>
                  <th class="text-center">Action</th>
                </tr>
              </thead>
              <tbody>
                  @foreach ($languages as $language)
                    <tr>
                      <td class="py-1 line-ellipsis">

                        <i class="{{$language->icon_code}} mr-25"></i>
                        {{$language->name}}
                      </td>
                      <td class="py-1">
                        <i class="bx bx-trending-up text-success align-middle mr-50"></i>
                        <span>{{strtoupper($language->shortcut)}}</span>
                      </td>
                      <td class="py-1">
                        @if ($language->dirction == 'ltr')
                          Left to right
                        @elseif ($language->dirction == 'rtl')
                          Right to left
                        @else
                          N/A
                        @endif
                      </td>

                      @if ($language->status == 1)
                          <td class="text-success py-1">Active</td>
                      @elseif($language->status == 0)
                          <td class="text-danger py-1">Inactive</td>
                      @else
                          <td class="text-success py-1">N/A</td>
                      @endif
                      

                      <td class="text-center py-1">
                        <div class="dropdown">
                          <span class="bx bx-dots-vertical-rounded font-medium-3 dropdown-toggle nav-hide-arrow cursor-pointer" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="menu"></span>
                          <div class="dropdown-menu dropdown-menu-right">
                            <div class="test">
                              <a class="dropdown-item" href="#"><i class="bx bx-edit-alt mr-1"></i>Edit Translation</a>
                              <a class="dropdown-item" href="{{route('admin.languages.editLang', $language->shortcut)}}"><i class="bx bx-trash mr-1"></i> Edit Language</a>
                            </div>
                          </div>
                        </div>
                      </td>
                    </tr>
                  @endforeach

              </tbody>
            </table>
            <!-- table ends -->
          <div class="ps__rail-x" style="left: 0px; bottom: 0px;"><div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps__rail-y" style="top: 0px; right: 0px;"><div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 0px;"></div></div></div>
        </div>
  </div>

</div>

@stop




{{-- Start Js pages --}}
@section('pages-js')

<script src="{{asset('static/backend/app-assets/js/scripts/modal/components-modal.min.js')}}"></script>
<script src="{{asset('static/backend/app-assets/js/scripts/popover/popover.min.js')}}"></script>
@stop{{-- End Js pages --}}
