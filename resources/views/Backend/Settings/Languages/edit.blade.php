{{-- Extends The Root --}}
@extends('backend.root')

{{-- Start Css Vendors --}}
@section('vendor-css')
<link rel="stylesheet" type="text/css" href="{{asset('static/backend/app-assets/vendors/css/tables/datatable/datatables.min.css')}}">
@stop {{-- End Css Vendors --}}


{{-- Start Pages Ttitle --}}

@section('pages-title')
Dashboard
@stop

{{-- End Pages Ttitle --}}





{{-- Start Page Content --}}
@section('content')


<div class="col-md-6 col-12">
      <div class="card">
        <div class="card-header">
          <h4 class="card-title d-flex justify-content-between align-items-center">
          	<span>{{$language->name}}</span>
          	<div>
          		<form method="POST" action="{{route('admin.languages.deleteLang', $language->shortcut)}}">
          			@csrf
          			<button type="submit" class="btn btn-light-danger btn-sm">
                  Delete
                </button>
          		</form>
          	</div>
          </h4>
        </div>
        <div class="card-content">
          <div class="card-body">
            <form class="form form-vertical" action="{{route('admin.languages.addNewLang')}}" method="POST">

            	@csrf 

              <div class="form-body">
                <div class="row">

                  <div class="col-12">
                    <div class="form-group">
                      <label for="first-name-icon">Language Name</label>
                      <div class="position-relative has-icon-left">
                        <input type="text" id="first-name-icon" class="form-control" name="name" value="{{$language->name}}" placeholder="Language Name">
                        <div class="form-control-position">
                          <i class='bx bx-globe'></i>
                        </div>
                        <small>(Ex: English)</small>
                      </div>
                    </div>
                  </div>

                  <div class="col-12">
                    <div class="form-group">
                      <label for="Short Form">Short Form</label>
                      <div class="position-relative has-icon-left">
                        <input type="text" id="Short Form" class="form-control" name="shortcut" value="{{$language->shortcut}}" placeholder="Short Form">
                        <div class="form-control-position">
                          <i class='bx bx-chevron-right' ></i>
                        </div>
                        <small>(Ex: en)</small>
                      </div>
                    </div>
                  </div>

                  <div class="col-12">
                    <div class="form-group">
                      <label for="Language Code">Language Code</label>
                      <div class="position-relative has-icon-left">
                        <input type="text" id="Language Code" class="form-control" name="code" value="{{$language->code}}" placeholder="Language Code">
                        <div class="form-control-position">
                          <i class='bx bx-chevron-right' ></i>
                        </div>
                        <small>(Ex: en_us)</small>
                      </div>
                    </div>
                  </div>

                  <div class="col-12">
                    <div class="form-group">
                      <label for="Language Icon Code">Language Icon Code</label>
                      <div class="position-relative has-icon-left">
                        <input type="text" id="Language Icon Code" class="form-control" name="icon_code" value="{{$language->icon_code}}" placeholder="Language Icon Code">
                        <div class="form-control-position">
                          <i class='bx bx-chevron-right' ></i>
                        </div>
                        <small>(Ex: flag-icon flag-icon-us) here you can find icon code: <a href="https://www.iso.org/obp/ui/" target="_blank">flag-icon-css</a> and here is the standards: <a href="http://flag-icon-css.lip.is/" target="_blank">ISO 3166-1-alpha-2</a></small>
                      </div>
                    </div>
                  </div>

                  <div class="col-12">
										<div class="form-group">
											<label>Language Dirction</label>
			                <select name="dirction" class="form-control">
			                   <option value="ltr" {{$language->dirction == 'ltr' ? 'selected' : ''}}>LTR (Left to Right)</option>
			                  <option value="rtl" {{$language->dirction == 'rtl' ? 'selected' : ''}}>RTL (Right to Left)</option>
			                </select>
			                <small>Content Dirction</small>
			              </div>
                  </div>

 
                  <div class="col-12">
                    <div class="form-group d-flex align-items-center">
                      <label>Status</label>
                      <div class="d-flex align-items-center ml-5 mt-50">
												<ul class="list-unstyled mb-0">
						              <li class="d-inline-block mr-5 mb-1">
						                <fieldset>
						                  <div class="radio radio-sm">
						                      <input type="radio" id="Active" name="status" value="1" {{$language->status == 1 ? 'checked' : ''}}>
						                      <label for="Active" class="cursor-pointer">Active</label>
						                  </div>
						                </fieldset>
						              </li>
						              <li class="d-inline-block mb-1">
						                <fieldset>
						                  <div class="radio">
						                      <input type="radio" id="Inactive" name="status" value="0" {{$language->status == 0 ? 'checked' : ''}}>
						                      <label for="Inactive" class="cursor-pointer">Inactive</label>
						                  </div>
						                </fieldset>
						              </li>
						            </ul>
                      </div>
                    </div>
                  </div>


                  <div class="col-12 d-flex justify-content-end">
                    <button type="submit" class="btn btn-light-secondary mr-1 mb-1">Save changes</button>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>



@stop


{{-- Start Js pages --}}
@section('pages-js')

<script src="{{asset('static/backend/app-assets/js/scripts/modal/components-modal.min.js')}}"></script>
<script src="{{asset('static/backend/app-assets/js/scripts/popover/popover.min.js')}}"></script>
@stop{{-- End Js pages --}}
