{{-- Extends The Root --}}
@extends('Backend.root')

{{-- Start Css Vendors --}}
@section('vendor-css')
<link rel="stylesheet" type="text/css" href="{{asset('static/backend/app-assets/vendors/css/charts/apexcharts.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('static/backend/app-assets/vendors/css/extensions/swiper.min.css')}}">
@stop {{-- End Css Vendors --}}

{{-- Start Css Pages --}}
@section('pages-css')
<link rel="stylesheet" type="text/css" href="{{asset('static/backend/app-assets/css/pages/dashboard-ecommerce.min.css')}}">
@stop {{-- End Css Pages --}}


{{-- Start Pages Ttitle --}}

@section('pages-title')
Dashboard
@stop

{{-- End Pages Ttitle --}}





{{-- Start Page Content --}}
@section('content')





{{App::getLocale()}}
<br>
{{current_lang('name')}}



<div class="col-xl-4 col-md-6 col-12 dashboard-latest-update">
      <div class="card">
        <div class="card-header d-flex justify-content-between align-items-center pb-50">
          <h4 class="card-title">Latest Update</h4>
          <div class="dropdown">
            <button class="btn btn-sm btn-outline-secondary dropdown-toggle" type="button" id="dropdownMenuButtonSec" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              2019
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButtonSec">
              <a class="dropdown-item" href="#">2019</a>
              <a class="dropdown-item" href="#">2018</a>
              <a class="dropdown-item" href="#">2017</a>
            </div>
          </div>
        </div>
        <div class="card-content">
          <div class="card-body p-0 pb-1 ps ps--active-y">
            <ul class="list-group list-group-flush">
              <li class="list-group-item list-group-item-action border-0 d-flex align-items-center justify-content-between">
                <div class="list-left d-flex">
                  <div class="list-icon mr-1">
                    <div class="avatar bg-rgba-primary m-0">
                      <div class="avatar-content">
                        <i class="bx bx-user text-primary font-size-base"></i>
                      </div>
                    </div>
                  </div>
                  <div class="list-content">
                    <a href="{{ADMIN_URI('users')}}" class="list-title">Total Users</a>
                    <small class="text-muted d-block">{{number_format($today_registered_users)}} users registered today</small>
                  </div>
                </div>
                <span>{{number_format($total_users)}}</span>
              </li>


              <li class="list-group-item list-group-item-action border-0 d-flex align-items-center justify-content-between">
                <div class="list-left d-flex">
                  <div class="list-icon mr-1">
                    <div class="avatar bg-rgba-success m-0">
                      <div class="avatar-content">
                        <i class="bx bx-shopping-bag text-success font-size-base"></i>
                      </div>
                    </div>
                  </div>
                  <div class="list-content">
                    <span class="list-title">Total Products</span>
                    <small class="text-muted d-block">13 products created today</small>
                  </div>
                </div>
                <span>1.25B</span>
              </li>
              <li class="list-group-item list-group-item-action border-0 d-flex align-items-center justify-content-between">
                <div class="list-left d-flex">
                  <div class="list-icon mr-1">
                    <div class="avatar bg-rgba-primary m-0">
                      <div class="avatar-content">
                        <i class="bx bx-strikethrough text-primary font-size-base"></i>
                      </div>
                    </div>
                  </div>
                  <div class="list-content">
                    <span class="list-title">Total Shops</span>
                    <small class="text-muted d-block">0 Shops created today</small>
                  </div>
                </div>
                <span>1.2k</span>
              </li>
              <li class="list-group-item list-group-item-action border-0 d-flex align-items-center justify-content-between">
                <div class="list-left d-flex">
                  <div class="list-icon mr-1">
                    <div class="avatar bg-rgba-danger m-0">
                      <div class="avatar-content">
                        <i class="bx bx-euro text-danger font-size-base"></i>
                      </div>
                    </div>
                  </div>
                  <div class="list-content">
                    <span class="list-title">Total Offers</span>
                    <small class="text-muted d-block">12 offers today</small>
                  </div>
                </div>
                <span>4.6k</span>
              </li>
            </ul>
          <div class="ps__rail-x" style="left: 0px; bottom: 0px;"><div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps__rail-y" style="top: 0px; height: 280px; right: 0px;"><div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 190px;"></div></div></div>
        </div>
      </div>
    </div>

@stop






{{-- Start Js Vendors --}}
@section('vendor-js')
<script src="{{asset('static/backend/app-assets/vendors/js/charts/apexcharts.min.js')}}"></script>
<script src="{{asset('static/backend/app-assets/vendors/js/extensions/swiper.min.js')}}"></script>
@stop{{-- End Js Vendors --}}

{{-- Start Js pages --}}
@section('pages-js')
<script src="{{asset('static/backend/app-assets/js/scripts/pages/dashboard-ecommerce.min.js')}}"></script>
@stop{{-- End Js pages --}}