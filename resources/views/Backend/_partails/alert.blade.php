@if (session('success'))

	<div class="alert alert-secondary alert-dismissible mb-2" role="alert">
	  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	    <span aria-hidden="true">×</span>
	  </button>
	  <div class="d-flex align-items-center">
	    <i class="bx bx-edit"></i>
	    <span>
	      {{session('success')}}
	    </span>
	  </div>
	</div>

@endif

@if (session('error'))

	<div class="alert alert-danger alert-dismissible mb-2" role="alert">
	  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	    <span aria-hidden="true">×</span>
	  </button>
	  <div class="d-flex align-items-center">
	    <i class="bx bx-edit"></i>
	    <span>
	      {{session('success')}}
	    </span>
	  </div>
	</div>
	
@endif