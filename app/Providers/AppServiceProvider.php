<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use App\Models\Backend\AppSetting as Setting;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        

        app()->singleton('lang', function(){
            $default_lang = Setting::where('name', '=', 'Language')->first();
            if (auth()->user())
            {
                if (empty(auth()->user()->lang))
                {
                    return $default_lang->value;
                }
                else
                {
                    return auth()->user()->lang;
                }
            }
            else
            {
                if (session()->has('lang'))
                {
                    return session()->get('lang');
                }
                else
                {
                    return $default_lang->value;
                }
            }
        });


        Schema::defaultStringLength(191);
    }
}
