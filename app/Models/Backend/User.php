<?php

namespace App\Models\Backend;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;

use DB;
use Cache;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];



    /**
    |
    |--------------------------------------------------------------------------
    | Check if user is online or offline
    |--------------------------------------------------------------------------
    |
    */
    public function isOnline()
    {
        return Cache::has('user-is-online-' . $this->id);
    }





    public function settings()
    {
        return $this->hasOne('App\Models\Pub\UserSetting', 'user_id');
        // $settings = DB::table('user_settings')->where('user_id', Auth::user()->id)->first();
        // return DB::table('user_settings')->get();
    }



    public function activities()
    {
        return $this->hasMany('App\Models\Pub\UserActivity', 'user_id');
        // $settings = DB::table('user_settings')->where('user_id', Auth::user()->id)->first();
        // return DB::table('user_settings')->get();
    }






    public function roles()
    {
        return $this->belongsToMany('App\Models\Backend\Role', 'user_role', 'user_id', 'role_id');
    }


    /**
    |--------------------------------------------------------------------------
    | Has any role?
    |--------------------------------------------------------------------------
    |
    | This method to check if current users has any role in the Role Table
    | or not.
    |
    */
    public function hasAnyRole($roles)
    {
        if (is_array($roles))
        {
            // Loop the index.
            foreach ($roles as $role) {
                if ($this->hasRole($role))
                {
                    return true;
                }
            }
        }

        else
        {          
            if ($this->hasRole($roles))
            {
                return true;
            }
        }

    }


    /**
    |--------------------------------------------------------------------------
    | Has role?
    |--------------------------------------------------------------------------
    |
    | This method to check if role exist in the Database
    | 
    |
    */
    public function hasRole($role)
    {
        if (is_array($role))
        {
            // Loop the index.
            foreach ($role as $rol) {
                if ($this->roles()->where('name', $rol)->first())
                {
                    return true;
                }
            }
        }

        else
        {          
            if ($this->roles()->where('name', $role)->first())
            {
                return true;
            }
        }

        return false;

        // if ($this->roles()->where('name', $role)->first())
        // {
        //     return true;
        // }

        // return false;
    }








}
