<?php

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    public function users()
    {
    	return $this->belongsToMany('App\Models\Backend\User', 'user_role', 'role_id', 'user_id');
    }
}
