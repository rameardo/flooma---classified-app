<?php

namespace App\Models\Pub;

use Illuminate\Database\Eloquent\Model;

class UserActivity extends Model
{
    protected $table = 'user_activities';
    
    public function user()
    {
        return $this->belongsTo('App\Models\Backend\User', 'id');
    }    
}
