<?php

namespace App\Models\Pub;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{

    public function translation()
    {
        return $this->hasMany('App\Models\Pub\Translation', 'lang_shortcut');
        // $settings = DB::table('user_settings')->where('user_id', Auth::user()->id)->first();
        // return DB::table('user_settings')->get();
    }

}
