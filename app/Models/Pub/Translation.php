<?php

namespace App\Models\Pub;

use Illuminate\Database\Eloquent\Model;

class Translation extends Model
{
    protected $table = 'translations';



    protected $fillable = [
        'id', 'lang_shortcut', 'key', 'value'
    ];
    
    public function lang()
    {
        return $this->belongsTo('App\Models\Pub\Language', 'shortcut');
    }    
}
