<?php

namespace App\Models\Pub;

use Illuminate\Database\Eloquent\Model;

class Visitor extends Model
{
		protected $table = 'visitors';

    // protected $fillable = [
    // 	'id','isp', 'country', 
    // 	'country_code', 'region_name', 'city', 
    // 	'zip', 'latitude', 'longitude',
    // 	'timezone', 'org', 'as', 'ip'
    // ];

    protected $fillable =
    [
    	'country'
    ];

    protected $dates = [
        'created_at',
        'updated_at'
    ];
}
