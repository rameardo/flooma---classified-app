<?php

namespace App\Models\Pub;

use Illuminate\Database\Eloquent\Model;

class UserSetting extends Model
{
		protected $table = 'user_settings';

    public function user()
    {
        return $this->belongsTo('App\Models\Backend\User', 'id');
    }
}
