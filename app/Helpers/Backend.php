<?php
use Illuminate\Contracts\Support\Arrayable;
use App\Models\Pub\UserActivity as Activity;
use App\Models\Pub\Language;
use App\Models\Pub\Translation;
use Illuminate\Support\Facades\DB;

// Admin Prefix
if (!function_exists('ADMIN_PREFIX'))
{
	function ADMIN_PREFIX()
	{
		return 'admin';
	}
}

// Admin uri
if (!function_exists('ADMIN_URI'))
{
	function ADMIN_URI($uri = null)
	{
		return url('admin/' . $uri);
	}
}

// Request Uri
if (!function_exists('REQUEST_URI'))
{
	function REQUEST_URI($uri = '')
	{
		return 'admin/' . $uri;
	}
}

//  Menu Active
if (!function_exists('isMenuActive'))
{
	function isMenuActive($uri, $className)
	{
		return request()->is($uri) ? $className : '';
	}
}


// Get User Public IP
if (!function_exists('public_ip'))
{
	function public_ip()
	{
		if (!empty($_SERVER["HTTP_CLIENT_IP"]))
		{
		//check for ip from share internet
		$ip = $_SERVER["HTTP_CLIENT_IP"];
		}
		elseif (!empty($_SERVER["HTTP_X_FORWARDED_FOR"]))
		{
		// Check for the Proxy User
		$ip = $_SERVER["HTTP_X_FORWARDED_FOR"];
		}
		else
		{
		$ip = $_SERVER["REMOTE_ADDR"];
		}
		// This will print user's real IP Address
		// does't matter if user using proxy or not.
		return $ip;
	}
}



//  Number Converter
if (!function_exists('number_format'))
{
	function number_format( $n, $precision = 1 ) {
		if ($n < 900) {
			// 0 - 900
			$n_format = number_format($n, $precision);
			$suffix = '';
		} else if ($n < 900000) {
			// 0.9k-850k
			$n_format = number_format($n / 1000, $precision);
			$suffix = 'K';
		} else if ($n < 900000000) {
			// 0.9m-850m
			$n_format = number_format($n / 1000000, $precision);
			$suffix = 'M';
		} else if ($n < 900000000000) {
			// 0.9b-850b
			$n_format = number_format($n / 1000000000, $precision);
			$suffix = 'B';
		} else {
			// 0.9t+
			$n_format = number_format($n / 1000000000000, $precision);
			$suffix = 'T';
		}
	  // Remove unecessary zeroes after decimal. "1.0" -> "1"; "1.00" -> "1"
	  // Intentionally does not affect partials, eg "1.50" -> "1.50"
		if ( $precision > 0 ) {
			$dotzero = '.' . str_repeat( '0', $precision );
			$n_format = str_replace( $dotzero, '', $n_format );
		}
		return $n_format . $suffix;
	}	
}

// Save Activity
if (!function_exists('logData'))
{
	function logData($userID, $signal, $fv=null, $fvl=null, $sv=null, $svl=null, $ai=null, $af=null, $au=null)
	{
		// New Activity object
		$activity = new Activity;

		// Activity Values
		$activity->user_id = $userID;
		$activity->signal = $signal;
		$activity->first_value = $fv;
		$activity->first_value_link = $fvl;
		$activity->second_value = $sv;
		$activity->second_value_link = $svl;
		$activity->activity_image = $ai;
		$activity->attached_file = $af;
		$activity->activity_url = $au;

		// Save the values
		$activity->save();
	}
}



// Get current language info
if (!function_exists('current_lang'))
{
	function current_lang($key=null)
	{
		$lang = Language::where('shortcut', App::getLocale())->get();

		if ($key === null or empty($key))
		{
			return $lang;
		}
		else
		{
			foreach ($lang as $langs) 
			{
				return $langs->$key;
			}			
		}

	}
}




// Language Function
if(!function_exists('__lang'))
{
	function __lang($key)
	{

		if (Translation::where([['key', '=', $key],['lang_shortcut', '=', App::getLocale()]])->count() > 0)
		{
			$trans = Translation::where([['key', '=', $key],['lang_shortcut', '=', App::getLocale()]])->get();

			foreach ($trans as $translation) 
			{
				if (empty($translation->value) or $translation->value === null)
				{
					return 'N/A';
				}
				else
				{
					return $translation->value;
				}
			}	
						
		}

		else
		{
			$trans = Translation::where([['key', '=', $key],['lang_shortcut', '=', 'en']])->get();
			if (count($trans) > 0)
			{
				foreach ($trans as $translation) 
				{
					if (empty($translation->value) or $translation->value === null)
					{
						return 'N/A';
					}
					else
					{
						return $translation->value;
					}
				}	
			}
			else
			{
				return 'N/A';
			}
		}

	}
}


// Save Default Language Values
if (!function_exists('defaultLangKey'))
{
	function defaultLangKey($key, $value)
	{
    DB::table('translations')->insert([
    	'lang_shortcut'	=> 'en',
    	'key'						=> $key,
    	'value'					=> $value,
    	'created_at'		=> now(),
    	'updated_at'		=> now()
    ]);		
	}
}


if(!function_exists('get_ip'))
{
	function get_ip()
	{
		if(isset($_SERVER['HTTP_CLIENT_IP']))
		{
			return $_SERVER['HTTP_CLIENT_IP'];
		}
		elseif(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
		{
			return $_SERVER['HTTP_X_FORWARDED_FOR'];
		}
		else
		{
			return (isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '');
		}
	}
}




if(!function_exists('visitors'))
{
	function visitors($key=null)
	{
		// $ip = '213.162.80.249'; Example...
		$ip = get_ip();
		$query = @unserialize(file_get_contents('http://ip-api.com/php/' . $ip));
		if ($key === null)
		{
			foreach ($query as $key => $value) {
				echo $value . ' ';
			}
		}
		else
		{
			if ($query && $query['status'] == 'success')
			{

				if (in_array($ip, $query)) {
					return $query[$key];
				}
				
				else
				{
					return 'N/A';
				}

			}
			else
			{
				return 'N/A';
			}			
		}

	}
}