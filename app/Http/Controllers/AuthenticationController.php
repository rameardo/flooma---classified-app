<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Backend\User;
use App\Models\Backend\Role;
use App\Models\Pub\UserSetting as Setting;
use App\Models\Pub\UserActivity as Activity;

class AuthenticationController extends Controller
{

    public function register(Request $request)
    {

    	// Create user
    	$user = new User;

        // Delete Spaces in the username value
        $username = str_replace(' ', '', $request->name);

        // Record the values
    	$user->name = $username;
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
    	$user->email = $request->email;
    	$user->password = bcrypt($request->password);
        $user->ip_address = public_ip();

    	// Save user
    	$user->save();

    	// set Role for this user
    	$user->roles()->attach(Role::where('name', 'User')->first());

        // Set Settings for this user
        $setting = new Setting;
        $setting->user_id = $user->id;
        $setting->save();

        // record this activity 
        logData($user->id, 'join', $user->first_name . ' ' . $user->last_name);

    	// Login this user
    	auth()->login($user);


    	// Redirect this user to home page
    	return redirect('/home');

    }
}
