<?php

namespace App\Http\Controllers\Backend\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Backend\User;
use App\Models\Backend\Role;
use Carbon\Carbon;

class DashboardController extends Controller
{
    public function index()
    {

    	$total_users = Role::where('name', 'User')->first()->users()->count();
    	
    	$today_registered_users= Role::where('name', 'User')
    													->first()->users()
    													->whereDate('users.created_at', Carbon::today())->count();

    	return view('Backend.Dashboard.index', compact('total_users', 'today_registered_users'));
    }
}
