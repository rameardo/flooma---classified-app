<?php

namespace App\Http\Controllers\Backend\Settings\Languages;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Pub\Language;
use App\Models\Pub\Translation;
use App\Models\Backend\AppSetting as Setting;

class LanguagesController extends Controller
{

    public function index()
    {
        $default_lang = Setting::where('name', '=', 'Language')->first();
        $languages = Language::all();
        return view('Backend.Settings.Languages.index', compact('languages', 'default_lang'));
    }

    public function setDefaultLanguage(Request $request)
    {
        $default_lang = Setting::where('name', '=', 'Language')->first();
        $default_lang->value = $request->default_lang;
        $default_lang->update();

        if ($default_lang->update())
        {
            return back();
        }
    }


    public function show($lang)
    {
        $language = Language::where('name', '=', $lang)->first();

        if ($language)
        {

            $translations = Translation::where('lang_shortcut', '=', $language->shortcut)->get();

            if ($translations) {
                return view('Backend.Settings.Languages.show', compact('language', 'translations'));
            }
            else
            {
                abort(404);
            }
        }

        else
        {
            abort(404);
        }
    }


    // New Language
    public function newLang()
    {
        return view('Backend.Settings.Languages.new');
    }

    // Add new Languages & Default Translation
    public function addNewLang(Request $request)
    {
        // Save Language
        $language = new Language;

        $language->name = $request->name;
        $language->shortcut = $request->shortcut;
        $language->code = $request->code;
        $language->icon_code = $request->icon_code;
        $language->dirction = $request->dirction;
        $language->status = $request->status;

        $language->save();

        // Save Default Translations
        $translation = Translation::where('lang_shortcut', '=', 'en')->get();
        
        foreach ($translation as $trans) {
            Translation::create([
                'lang_shortcut' => $language->shortcut,
                'key' => $trans->key,
                'value' => $trans->value
            ]);
        }
   
        return redirect(route('admin.languages.index'));
    }

    // Edit Language
    public function editLang($lang)
    {
        $language = Language::where('shortcut', '=', $lang)->first();
        if ($language)
        {
            return view('Backend.Settings.Languages.edit', compact('language'));
        }
        else
        {
            abort(404);
        }
        
    }

    // Delete Language & Translations
    public function deleteLang($lang)
    {
        // Delete Language
        $language = Language::where('shortcut', '=', $lang)->delete();   


        // Delete Language Translations
        $translation = Translation::where('lang_shortcut', '=', $lang)->delete();

        // If successfully deleted return to language index
        if ($translation && $language)
        {
            return redirect(route('admin.languages.index'));
        }
        // If not successfully deleted return error message.
        else
        {
            echo 'error by delete.';
        }
        
    }


}
