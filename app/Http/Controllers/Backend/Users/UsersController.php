<?php

namespace App\Http\Controllers\Backend\Users;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Backend\User;
use App\Models\Backend\Role;
use App\Models\Pub\UserSetting as Setting;
use App\Models\Pub\UserActivity as Activity;
// use App\Models\Public\UserSetting;

use DB;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = Role::where('name', 'User')->first()->users()->get();  
        $users_counter = Role::where('name', 'User')->first()->users()->count();  

        return view('Backend.Users.index', compact('users', 'users_counter'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // $where = array('id' => $id);
        // $data['product_info'] = Product::where($where)->first();
        $user = Role::where('name', 'User')
        ->first()
        ->users()
        ->where('users.id', $id)
        ->first();

        
        if ($user)
        {
            $activity_count = Activity::where('user_id', $user->id)->count();
            $activities = Activity::where('user_id', $user->id)->paginate(5);            
            return view('Backend.Users.show', compact('user', 'activity_count', 'activities'));
        }
        else
        {
            abort(404);
        }
    }

    public function showActivity($id)
    {
        $user = Role::where('name', 'User')
        ->first()
        ->users()
        ->where('users.id', $id)
        ->first();

        $activity_count = Activity::where('user_id', $user->id)->count();  

        if ($user)
        {
            return view('Backend.Users.activity', compact('user', 'activity_count'));
        }
        else
        {
            abort(404);
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = Role::where('name', 'User')
        ->first()
        ->users()
        ->where('users.id', $id)
        ->first();

        
        if ($user)
        {
            return view('Backend.Users.edit', compact('user'));
        }
        else
        {
            abort(404);
        }
    }


    public function account_settings(Request $request)
    {

        $user = Role::where('name', 'User')
        ->first()
        ->users()
        ->where('users.id', $request->user_id)
        ->first();

       $setting = Setting::find($request->user_id);


       if ($user)
       {
        $user->is_verified = $request->is_verified ? 1 : 0 ?? 0;
        $user->is_active = $request->is_active ? 1 : 0 ?? 0;

        dd($user);
       }

       if ($setting)
       {

        // Get actions section values
        $setting->can_user_create_shop = $request->can_user_create_shop ? 1 : 0 ?? 0;
        $setting->can_user_sell = $request->can_user_sell ? 1 : 0 ?? 0;
        $setting->can_user_buy = $request->can_user_buy ? 1 : 0 ?? 0;
        $setting->can_user_comment = $request->can_user_comment ? 1 : 0 ?? 0;
        $setting->can_user_review = $request->can_user_review ? 1 : 0 ?? 0;
        $setting->can_user_contact_support_team = $request->can_user_contact_support_team ? 1 : 0 ?? 0;
        $setting->can_visitor_show_user_shop = $request->can_visitor_show_user_shop ? 1 : 0 ?? 0;
        
        // Save Data
        // dd($setting);
        $setting->save();
       }

       return back();




       // 
       // $setting->can_user_create_shop = $can_user_create_shop;

       // $setting->save();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
