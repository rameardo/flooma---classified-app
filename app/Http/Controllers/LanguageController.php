<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pub\Language;
use App\Models\Backend\AppSetting as Setting;
class LanguageController extends Controller
{
	public function index($lang)
	{

		$default_lang = Setting::where('name', '=', 'Language')->first();
		
		if (in_array($lang, json_decode(json_encode(Language::pluck('shortcut')), true)))
		{
			if (auth()->user())
			{
				$user = auth()->user();
				$user->lang = $lang;
				$user->save();
			}
			else
			{
				if(session()->has('lang'))
				{
					session()->forget('lang');
				}
				session()->put('lang', $lang);
			}
			
		}
		else
		{
			if (auth()->user())
			{
				$user = auth()->user();
				$user->lang = $default_lang->value;
				$user->save();
			}
			else
			{
				if(session()->has('lang'))
				{
					session()->forget('lang');
				}
				session()->put('lang', $lang);
			}		
			session()->put('lang', $default_lang->value);
		}

		return back();


	}
}
