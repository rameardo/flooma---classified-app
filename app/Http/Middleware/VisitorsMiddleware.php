<?php

namespace App\Http\Middleware;

use Closure;

use App\Models\Pub\Visitor;

class VisitorsMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // echo visitors('query');

        $visitor = Visitor::where('ip', '=', visitors('query'))->first();

        if ($visitor === null) {
          $insert = new Visitor;
          $insert->isp = visitors('isp');
          $insert->country = visitors('country');
          $insert->country_code = visitors('countryCode');
          $insert->region_name = visitors('regionName');
          $insert->city = visitors('city');
          $insert->zip = visitors('zip');
          $insert->latitude = visitors('lat');
          $insert->longitude = visitors('lon');
          $insert->timezone = visitors('timezone');
          $insert->org = visitors('org');
          $insert->as = visitors('as');
          $insert->ip = visitors('query');
          $insert->save();
        }
        // $visitors = Visitor::updateOrCreate(
        //     ['isp' => visitors('isp')],
        //     ['country' => visitors('country')],
        //     // ['country_code' => visitors('countryCode')],
        //     // ['region_name' => visitors('regionName')],
        //     // ['city' => visitors('city')],
        //     // ['zip' => visitors('zip')],
        //     // ['latitude' => visitors('lat')],
        //     // ['longtitude' => visitors('lon')],
        //     // ['timezone' => visitors('timezone')],
        //     // ['org' => visitors('org')],
        //     // ['as' => visitors('as')],
        //     ['ip' => visitors('query')]
        // );

        // dd($visitors);
        return $next($request);
    }
}
