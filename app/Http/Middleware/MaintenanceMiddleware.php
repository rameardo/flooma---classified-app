<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Response;

use App\Models\Backend\AppSetting as Setting;

class MaintenanceMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $setting = Setting::where('name', '=', 'Maintenance')->first();

        if ($setting->value == 1)
        {
           
            return new response(view('Handling.Maintenance'));
            
            // return redirect('/main');
            
        }
        else
        {
            return $next($request);
        }

        
    }
}
