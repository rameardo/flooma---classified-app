<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_settings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->nullable();
            $table->boolean('can_user_create_shop')->default(false)->nullable();
            $table->boolean('can_user_sell')->default(true)->nullable();
            $table->boolean('can_user_buy')->default(true)->nullable();
            $table->boolean('can_user_comment')->default(true)->nullable();
            $table->boolean('can_user_review')->default(true)->nullable();
            $table->boolean('can_visitor_show_user_shop')->default(false)->nullable();
            $table->boolean('can_user_contact_support_team')->default(true)->nullable();
            $table->boolean('own_store')->default(false)->nullable();
            $table->boolean('membership_type')->default(false)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_settings');
    }
}
