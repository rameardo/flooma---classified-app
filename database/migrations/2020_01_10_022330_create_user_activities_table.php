<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserActivitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_activities', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id');
            $table->string('signal')->nullable();
            $table->text('first_value')->nullable();
            $table->text('first_value_link')->nullable();
            $table->text('second_value')->nullable();
            $table->text('second_value_link')->nullable();
            $table->text('activity_image')->nullable();
            $table->text('attached_file')->nullable();
            $table->text('activity_url')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_activities');
    }
}
