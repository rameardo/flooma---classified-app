<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DefaultAppSettings extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	// Maintenance Setting
        DB::table('app_settings')->insert([
            'name' => 'Maintenance',
            'description' => 'This is website Maintenance mode.',
            'value'	=> '0',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    	
        // Default Site Language
        DB::table('app_settings')->insert([
            'name' => 'Language',
            'description' => 'This is Site Default Language',
            'value' => 'en',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        


    }
}
