<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;



class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    	// Admin Role
        DB::table('roles')->insert([
            'name' => 'Admin',
            'description' => 'This is Admin Role',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    	
    	// Editor Role
        DB::table('roles')->insert([
            'name' => 'Editor',
            'description' => 'This is Editor Role',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    	
    	// User Role
        DB::table('roles')->insert([
            'name' => 'User',
            'description' => 'This is User Role',
            'created_at' => now(),
            'updated_at' => now(),
        ]);


    }
}
