<?php

use Illuminate\Database\Seeder;

class DefaultTranslation extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Start Sidebar default Translations
        defaultLangKey('dashboard', 'Dashboard');

        defaultLangKey('user_management', 'User Management');
        defaultLangKey('users', 'Users');
        defaultLangKey('profile', 'Profile');
        defaultLangKey('pending_users', 'Pending Users');
        defaultLangKey('deleted_users', 'Deleted Users');
        defaultLangKey('products', 'Products');
        defaultLangKey('categories', 'Categories');
        defaultLangKey('sold_products', 'Sold Products');
        defaultLangKey('hidden_products', 'Hidden Products');
        defaultLangKey('pending_products', 'Pending Products');
        defaultLangKey('deleted_products', 'Deleted Products');
        defaultLangKey('shops', 'Shops');
        defaultLangKey('pending_shops', 'Pending Shops');
        defaultLangKey('deleted_shops', 'Deleted Shops');
        defaultLangKey('reviews', 'Reviews');
        defaultLangKey('deleted_reviews', 'Deleted Reviews');
        defaultLangKey('comments', 'Comments');
        defaultLangKey('messages', 'Messages');
        defaultLangKey('deleted_messages', 'Deleted Messages');
        defaultLangKey('offers', 'Offers');

        defaultLangKey('site_management', 'Site Management');
        defaultLangKey('administrators', 'Administrators');
        defaultLangKey('pending_admins', 'Pending Admins');
        defaultLangKey('deleted_admins', 'Deleted Admins');
        defaultLangKey('blog', 'Blog');
        defaultLangKey('articles', 'Articles');
        defaultLangKey('tags', 'Tags');
        defaultLangKey('pages', 'Pages');
        defaultLangKey('newslatter', 'Newlatter');
        defaultLangKey('contact_messages', 'Contact Messages');

        defaultLangKey('technical', 'Technical');
        defaultLangKey('project_issues', 'Project Issues');
        defaultLangKey('issues', 'Issues');
        defaultLangKey('administrators_todo', 'Administrators Todo');
        defaultLangKey('todos', 'Todos');
        defaultLangKey('system_operational', 'System Operational');

        defaultLangKey('settings', 'Settings');
        defaultLangKey('general_settings', 'General Settings');
        defaultLangKey('languages_settings', 'Languages Settings');
        defaultLangKey('email_settings', 'E-Mail Settings');
        defaultLangKey('payments_settings', 'Payment Settings');
        defaultLangKey('visual_settings', 'Visual Settings');
        defaultLangKey('prefernce_settings', 'Prefernce Settings');
        defaultLangKey('currency_settings', 'Currency Settings');
        defaultLangKey('social_login_settings', 'Social Login Settings');
        defaultLangKey('api_settings', 'API Settings');
        // End Sidebar default Translations



        // Start Dashboard default Translations
        defaultLangKey('last_update', 'Last Update');
        defaultLangKey('total_users', 'Total users');
        defaultLangKey('users_registered_today', 'users registered today');
        defaultLangKey('total_products', 'Total Products');
        defaultLangKey('products_created_today', 'products created today');
        defaultLangKey('total_shops', 'Total Shops');
        defaultLangKey('Shops_created_today', 'Shops created today');
        defaultLangKey('total_offers', 'Total Offers');
        defaultLangKey('offers_today', 'offers today');
        // End Dashboard default Translations
        
    }
}
