<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DefaultLanguage extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('languages')->insert([
            'name' => 'English',
            'shortcut' => 'en',
            'code' => 'en_us',
            'icon_code' => 'flag-icon flag-icon-us',
            'position' => 1,
            'status'	=> 1,
            'dirction'	=> 'ltr',
            'created_at'	=> now(),
            'updated_at'	=> now(),
        ]);
    }
}
