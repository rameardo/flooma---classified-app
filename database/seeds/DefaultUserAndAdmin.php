<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DefaultUserAndAdmin extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            'name' => 'Admin',
            'description' => 'This is Admin Role',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }
}
